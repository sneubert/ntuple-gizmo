/**
  @date:   2017-05-12
  @author: mstahl
  @brief:  TMVA traning wrapper using boost property trees
*/
//ROOT
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
//TMVA
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Config.h"
#include "TMVA/DataLoader.h"
#include "TMVA/CrossValidation.h"
//STL
#include <vector>
#include <iostream>
#include <string>
//local
#include <IOjuggler.h>
#include "../include/EnvParser.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  //get some stuff, IOjuggler takes care of this
  //parse options passed to executable
  auto options = IOjuggler::parse_options(argc, argv, "c:d:o:p:hv:");
  MessageService msgsvc("trainMVA",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  //get ptree
  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //Create a ROOT output file where TMVA will store ntuples, histograms, etc.
  auto wd = options.get<std::string>("workdir");
  const auto ofn = wd+"/"+options.get<std::string>("outfilename");
  IOjuggler::dir_exists(ofn,msgsvc);
  TFile* outputFile = TFile::Open(ofn.data(),"RECREATE");

  //replace {prefix} in config tree
  IOjuggler::replace_stuff_in_ptree(configtree,"{prefix}",options.get<std::string>("prefix",""),"");
  //append and replace stuff in ptree
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);

  //lambda for testing and training individual tasks
  auto train_and_test = [&msgsvc,&wd,outputFile] (pt::ptree& taskconfig, const std::string& factory_name) {

    TStopwatch task_clock;
    task_clock.Start();

    //set output directory of xml and class file
    if(wd != "."){
      TMVA::Tools::Instance();
      TMVA::gConfig().GetIONames().fWeightFileDir = wd;
    }

    //ParseEnvName declared in EnvParser.h
    auto sig_filename = ParseEnvName(taskconfig.get<std::string>("sig_filename"));
    auto bkg_filename = ParseEnvName(taskconfig.get<std::string>("bkg_filename"));
    auto sig_treename = taskconfig.get<std::string>("sig_treename");
    auto bkg_treename = taskconfig.get<std::string>("bkg_treename");

    msgsvc.infomsg("Input files for this task: ");
    msgsvc.infomsg("sig_filename: " + sig_filename);
    msgsvc.infomsg("sig_treename: " + sig_treename);
    msgsvc.infomsg("bkg_filename: " + bkg_filename);
    msgsvc.infomsg("bkg_treename: " + bkg_treename);

    auto sig_input = IOjuggler::get_file(sig_filename,wd);
    auto bkg_input = IOjuggler::get_file(bkg_filename,wd);

    auto sig_tree = IOjuggler::get_obj<TTree>(sig_input,sig_treename);
    auto bkg_tree = IOjuggler::get_obj<TTree>(bkg_input,bkg_treename);

    //check if input variables exist. skip them, if not
    auto vars = taskconfig.get_child_optional("variables");
    if(!vars)
      throw std::runtime_error("\"variables\" node is missing. Aborting");
    auto specs = taskconfig.get_child_optional("spectators");

    if(auto ignm = taskconfig.get_optional<bool>("ignore_missing_variables"); *ignm)
      for(const auto& var : *vars){
        if(sig_tree->GetLeaf((var.first).data()) == nullptr || bkg_tree->GetLeaf((var.first).data()) == nullptr){
          msgsvc.warningmsg("Variable \"" + var.first + "\" not found in signal or background tree. Removing it from training");
          (*vars).erase(var.first);
        }
      if(specs)
        for(const auto& spec : *specs)
          if(sig_tree->GetLeaf((spec.first).data()) == nullptr || bkg_tree->GetLeaf((spec.first).data()) == nullptr){
            msgsvc.warningmsg("Spectator variable \"" + spec.first + "\" not found in signal or background tree. Removing it from training");
            (*specs).erase(spec.first);
          }
      }

    //add friend tree(s) and push them into a vector because they will go out of scope
    std::vector< TFriendElement* > fes;
    if(const auto sffs = taskconfig.get_child_optional("sig_friendfiles"))
      for(const auto& sff : *sffs){
        const auto sffn = sff.second.get<std::string>("filename"), sftn = sff.second.get<std::string>("treename");
        fes.push_back(sig_tree->AddFriend(sftn.data(),IOjuggler::get_file(sffn,wd)));
        msgsvc.infomsg("Added signal friend tree "+sftn+" from file "+sffn+" in working directory "+wd);
      }
    if(const auto bffs = taskconfig.get_child_optional("bkg_friendfiles"))
      for(const auto& bff : *bffs){
        const auto bffn = bff.second.get<std::string>("filename"), bftn = bff.second.get<std::string>("treename");
        fes.push_back(bkg_tree->AddFriend(bftn.data(),IOjuggler::get_file(bffn,wd)));
        msgsvc.infomsg("Added signal friend tree "+bftn+" from file "+bffn+" in working directory "+wd);
      }

    auto load_data = [&] (auto& data_loader) -> void {
      //add input variables to train MVA
      for(const auto& var : *vars)
        data_loader.AddVariable(var.first, var.second.get("label",var.first), var.second.get("unit"," "),
                                      static_cast<char>(var.second.get("type",'F')),var.second.get("low",0.),var.second.get("high",0.));

      //add spectator variables to train MVA
      if(specs)
        for(const auto& spec : *specs)
          data_loader.AddSpectator(spec.first,spec.second.get("label",spec.first),spec.second.get("unit"," "),
                                         spec.second.get("low",0.),spec.second.get("high",0.));

      //get signal and background trees, add them to the factory
      data_loader.AddSignalTree    (sig_tree);
      data_loader.AddBackgroundTree(bkg_tree);

      //use weights
      if(auto sig_weight = taskconfig.get_optional<std::string>("sig_weightname"))
        data_loader.SetSignalWeightExpression(*sig_weight);
      if(auto bkg_weight = taskconfig.get_optional<std::string>("bkg_weightname"))
        data_loader.SetBackgroundWeightExpression(*bkg_weight);

      //Prepare Testing and Training. Apply additional cuts on the signal and background samples (can be different)
      //ParseEnvName declared in EnvParser.h
      data_loader.PrepareTrainingAndTestTree(TCut(taskconfig.get("sig_cut","").data()),
                                                   TCut(taskconfig.get("bkg_cut","").data()),
                                                   ParseEnvName(taskconfig.get<std::string>("traintestopt")));
    };

    if(taskconfig.get("CrossValidation",0)){
      msgsvc.infomsg("Running CrossValidation");
      TMVA::DataLoader dataloader("");
      load_data(dataloader);
      TMVA::CrossValidation cv(factory_name, &dataloader, outputFile, taskconfig.get<std::string>("factory_option"));
      //book TMVA methods
      for(const auto& meth : taskconfig.get_child("method"))
        cv.BookMethod(static_cast<TMVA::Types::EMVA>(meth.second.get<int>("type")), meth.first, meth.second.get<std::string>("option"));
      cv.Evaluate();
    }
    else{
      //Create TMVA factory which handles the training and testing
      TMVA::Factory factory(factory_name, outputFile, taskconfig.get<std::string>("factory_option"));
      TMVA::DataLoader dataloader("");
      load_data(dataloader);
      //book TMVA methods
      for(const auto& meth : taskconfig.get_child("method"))
        factory.BookMethod(&dataloader, static_cast<TMVA::Types::EMVA>(meth.second.get<int>("type")), meth.first, meth.second.get<std::string>("option"));

      // Train MVAs using the set of training events
      factory.TrainAllMethods();
      // ---- Evaluate all MVAs using the set of test events
      factory.TestAllMethods();
      // ----- Evaluate and compare performance of all configured MVAs
      factory.EvaluateAllMethods();
    }

    task_clock.Stop();
    if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)task_clock.Print();

  };

  //loop all tasks individually for training and testing
  for(auto& task : configtree.get_child("tasks"))
    train_and_test(task.second,task.first);

  msgsvc.infomsg(TString::Format("==> View results with:\nTMVA::TMVAGui(\"%s\")",outputFile->GetName()));
  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)clock.Print();

}
