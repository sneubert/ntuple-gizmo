/**
  @date:   2017-05-12
  @author: mstahl
  @brief:  Evaluate TMVA weight files using boost property trees
*/
//ROOT
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TObjArray.h"
#include "TString.h"
#include "TFriendElement.h"
//TMVA
#include "TMVA/Reader.h"
//STL
#include <iostream>
#include <vector>
#include <cmath>
//BOOST
#include <boost/property_tree/ptree.hpp>
//local
#include <ProgressBar.h>
#include <IOjuggler.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  //parse inputs and get corresponding objects
  const auto options = IOjuggler::parse_options(argc, argv, "c:d:i:o:p:t:hm:f:v:","nonoptions: any number of <file(list):friendtree> combinations");
  MessageService msgsvc("applyMVA",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto wd            = options.get<std::string>("workdir");
  const auto treename      = options.get<std::string>("treename");
  const auto wfname        = options.get<std::string>("weightfile");
  const auto factory_name  = options.get<std::string>("factoryname");
  const auto ofn           = options.get<std::string>("outfilename");
  const auto tree          = IOjuggler::get_chain(treename, options.get<std::string>("infilename"), wd, msgsvc);


  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //replace {prefix} in config tree
  IOjuggler::replace_stuff_in_ptree(configtree,"{prefix}",options.get<std::string>("prefix",""),"");

  //add friend tree(s)
  [[maybe_unused]] auto fes = IOjuggler::get_friends(options,tree,msgsvc);
  tree->SetBranchStatus("*",0);

  //read training variables and spectators from config file
  const auto vars = configtree.get_child("tasks." + factory_name + ".variables");
  const unsigned int nvars = static_cast<unsigned int>(vars.size());

  const auto specs = configtree.get_child_optional("tasks." + factory_name + ".spectators");
  unsigned int nspec = 0;
  if(specs) nspec=static_cast<unsigned int>((*specs).size());

  //prepare tree and MVA reader
  std::vector<TLeaf*> var_leaves(nvars+nspec,nullptr);
  TMVA::Reader* reader = new TMVA::Reader();
  std::vector<float> MVAvarvals(nvars+nspec,0.f);

  unsigned int var_counter = 0u;
  for(const auto& var : vars){
    tree->SetBranchStatus(var.first.data(),1);
    var_leaves[var_counter] = tree->GetLeaf(var.first.data());
    if(configtree.get("tasks." + factory_name + ".ignore_missing_variables",false))
      if(var_leaves[var_counter] == nullptr){
        msgsvc.warningmsg("Variable \"" + var.first + "\" not found in input tree. Skipping it");
        continue;
      }
    reader->AddVariable(var.first.data(),&MVAvarvals[var_counter]);
    var_counter++;
  }

  // add spectators
  if(specs)
    for(const auto& var : *specs){
      tree->SetBranchStatus(var.first.data(),1);
      var_leaves[var_counter] = tree->GetLeaf(var.first.data());
      if(configtree.get("tasks." + factory_name + ".ignore_missing_variables",false))
        if(var_leaves[var_counter] == nullptr){
          msgsvc.warningmsg("Variable \"" + var.first + "\" not found in input tree. Skipping it");
          continue;
        }
      reader->AddSpectator(var.first.data(),&MVAvarvals[var_counter]);
      var_counter++;
    }
  var_leaves.resize(var_counter);
  MVAvarvals.resize(var_counter);

  reader->BookMVA("myMVA",wd+"/"+wfname);

  //make new friend tree. we'll add the tree later as friend to `tree`, so that one has direct access to the new variables via the old tree later
  TFile* ff = TFile::Open((wd+"/"+ofn).data(),"RECREATE");
  TTree TF((treename+"_MVAfriend").data(),(treename+"_MVAfriend").data());
  double response;
  TF.Branch(configtree.get<std::string>("response_name").data(),&response,(configtree.get<std::string>("response_name") + "/D").data());

  //make functor for response (so that you don't need an if statement for every event)
  std::function<double(TMVA::Reader*)> response_functor;
  if(configtree.get("rarity",false))
    response_functor = [] (TMVA::Reader* reader) {
      return reader->GetRarity("myMVA");
    };
  else
    response_functor = [] (TMVA::Reader* reader) {
      return reader->EvaluateMVA("myMVA");
    };

  msgsvc.infomsg("Looping tree and calculating MVA output");
  auto nentries = tree->GetEntries();
  ProgressBar pg;
  pg.start(nentries,msgsvc);

  for (decltype(nentries) evt = 0; evt < nentries; evt++){
    tree->GetEntry(evt);
    pg.update(evt);
    for(unsigned int i = 0u; i < var_counter; i++)
      MVAvarvals[i] = var_leaves[i]->GetValue(0);
    response = response_functor(reader);
    TF.Fill();
  }
  const int time = pg.stop();
  msgsvc.infomsg("Elapsed time for applying BDT: "+std::to_string(time)+" s");

  TF.Write();
  TF.Print();
  ff->Close();

  tree->SetBranchStatus("*",true);//re-enable the branches

  delete reader;

  return 0;
}
