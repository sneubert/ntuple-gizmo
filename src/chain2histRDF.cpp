//C++
#include <type_traits>
#include <vector>
#include <array>
#include <map>
#include <exception>
#include <string>
#include <sstream>
#include <functional>
#include <chrono>
#include <thread>
//root
#include <TROOT.h>
#include <TObject.h>
#include <TH1.h>
#include <TFile.h>
#include <TString.h>
#include <TChain.h>
#include <TFriendElement.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TEntryList.h>
#include <TEnv.h>
//new RDF-stuff
#include <ROOT/RDataFrame.hxx>
#include <ROOT/RDF/RInterface.hxx>
//local
#include "ProgressBar.h"
#include "IOjuggler.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

struct hdef {
  std::string               name;
  std::string               wght;
  std::array<std::string,3> vars;
  std::array<int,3>         bins;
  std::array<double,3>      mins;
  std::array<double,3>      maxs;
  //default ctor
  hdef(){
    name = "htemp";
    wght = "";
    vars = {"","",""};
    bins = {128,128,128};
    mins = {0.,0.,0.};
    maxs = {0.,0.,0.};
  }
};

int main(int argc, char** argv){

  //parse command line options
  const auto options = IOjuggler::parse_options(argc, argv, "c:d:i:o:t:v:p:h","nonoptions: any number of <file(list):friendtree> combinations");
  const auto wd = options.get<std::string>("workdir");
  const auto ofn = options.get<std::string>("outfilename");
  const auto ifn = options.get<std::string>("infilename");
  const auto itn = options.get<std::string>("treename");

  //init MessageService for easy and nice printout
  MessageService msgsvc("chain2histRDF",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));

  //get config-file
  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //  replace {prefix} in config tree
  IOjuggler::replace_stuff_in_ptree(configtree,"{prefix}",options.get<std::string>("prefix",""),"");
  //append and replace stuff in ptree
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);

  //now we can get the input file and set up the dataframe
  //first, we need to know how many events there are and how many threads we use:
  ROOT::EnableImplicitMT();//need to call this first to be able to call ROOT::GetImplicitMTPoolSize()
  const auto nSlots = std::min(configtree.get("threads",ROOT::GetImplicitMTPoolSize()),ROOT::GetImplicitMTPoolSize());
  msgsvc.debugmsg("Running with " + std::to_string(nSlots) + " parallel threads");
  ROOT::EnableImplicitMT(nSlots);

  //get input as TChain
  auto chain = IOjuggler::get_chain(itn,ifn,wd,msgsvc);
  //attach friend-files, if any
  msgsvc.debugmsg("Trying to invite some friends");
  [[maybe_unused]] auto fes = IOjuggler::get_friends(options,chain,msgsvc);

  ROOT::RDataFrame d(*chain.get());
  //use ROOT::RDF::RNode to chain commands on the dataframe
  ROOT::RDF::RNode df = d;

  msgsvc.infomsg("Request to save histograms defined in " + options.get<std::string>("config") +
                 " from TTree " + itn + " in " + wd + "/" + ifn + " to " + wd + "/" + ofn);

  auto nh = configtree.get_child("hists").size();
  msgsvc.infomsg("Requested to produce " + std::to_string(nh) + " histograms");

  //call define action (do this here. the new column should be available for all hists in the following)
  if(const auto de = configtree.get_child_optional("Define")){
    for(const auto& def : *de){
      msgsvc.infomsg("Calling define action. New column: " + def.first + ", defined by "+ def.second.get_value<std::string>());
      df = df.Define(def.first,def.second.get_value<std::string>());
    }
  }
  //global cut
  if(const auto filter = configtree.get_optional<std::string>("Filter")){
    msgsvc.infomsg("Applying global Filter " + *filter);
    df = df.Filter(*filter);
  }

  //make a vector of RResultPtr to histos (they trigger the event loop when Get is called on the object pointer,
  // but the RResultPtr is more than just a shared_ptr, which makes it un-castable to the TH1 base class. Therefore we have to explicitly use all three forms)
  std::vector<ROOT::RDF::RResultPtr<TH1D>> lazy_1Dhistos;
  std::vector<ROOT::RDF::RResultPtr<TH2D>> lazy_2Dhistos;
  std::vector<ROOT::RDF::RResultPtr<TH3D>> lazy_3Dhistos;

  auto make_hist = [&lazy_1Dhistos,&lazy_2Dhistos,&lazy_3Dhistos,&msgsvc] (const auto&& dim, hdef&& hp, ROOT::RDF::RNode& adf) -> void {
    if(hp.wght.empty()){//it's not pretty like this, but should perform better than using a default weight of 1.
      switch(dim){
        case 1: lazy_1Dhistos.push_back(
                adf.Histo1D(ROOT::RDF::TH1DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0]},hp.vars[0])
              );
          break;
        case 2: lazy_2Dhistos.push_back(
                adf.Histo2D(ROOT::RDF::TH2DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0],
                                                 hp.bins[1],hp.mins[1],hp.maxs[1]},hp.vars[0],hp.vars[1])
              );
          break;
        case 3: lazy_3Dhistos.push_back(
                adf.Histo3D(ROOT::RDF::TH3DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0],
                                                 hp.bins[1],hp.mins[1],hp.maxs[1],hp.bins[2],hp.mins[2],hp.maxs[2]},
                            hp.vars[0],hp.vars[1],hp.vars[2])
              );
          break;
        default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one...");
          break;
      }
    }
    else{
      switch(dim){
        case 1: lazy_1Dhistos.push_back(
                adf.Histo1D(ROOT::RDF::TH1DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0]},hp.vars[0],hp.wght)
              );
          break;
        case 2: lazy_2Dhistos.push_back(
                adf.Histo2D(ROOT::RDF::TH2DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0],
                                                 hp.bins[1],hp.mins[1],hp.maxs[1]},hp.vars[0],hp.vars[1],hp.wght)
              );
          break;
        case 3: lazy_3Dhistos.push_back(
                adf.Histo3D(ROOT::RDF::TH3DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0],
                                                 hp.bins[1],hp.mins[1],hp.maxs[1],hp.bins[2],hp.mins[2],hp.maxs[2]},
                            hp.vars[0],hp.vars[1],hp.vars[2],hp.wght)
              );
          break;
        default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one...");
          break;
      }
    }
  };

  for (const auto& hist : configtree.get_child("hists")){
    ROOT::RDF::RNode adf = df;
    //apply cuts
    if(const auto cut = hist.second.get_optional<std::string>("cut")){
      msgsvc.debugmsg("Applying Filter " + *cut);
      adf = adf.Filter(*cut);
    }
    //call histo lazy actions
    //decide whether to call Histo1D, Histo2D or Histo3D based on string parsing
    //we have to parse a Draw command here. do it like before, titles etc. can be passed later in draw_stuff
    if (const auto drawcmd = hist.second.get_optional<std::string>("Draw")){
      //a Draw command can look like: zvar:yvar:xvar>>hist_name(<nbinsx>,<xlow>,<xhi>,<nbinsy>,<ylow>,<yhi>,<nbinsz>,<zlow>,<zhi>) (note the order!)
      //strip everything right of >> to get the variables we want to plot
      auto pos = (*drawcmd).find(">>");//14 in the example string
      auto varsstr = pos == std::string::npos ? *drawcmd : boost::algorithm::erase_tail_copy(*drawcmd,(*drawcmd).size()-pos);//zvar:yvar:xvar
      std::vector<std::string> vars;
      boost::algorithm::split(vars, varsstr , boost::algorithm::is_any_of(":"));
      msgsvc.debugmsg("First variable of this \"Draw\" command "+vars[0]);
      const auto dim = vars.size();
      msgsvc.debugmsg("Drawing " + std::to_string(dim) + "D histogram");
      //start to fill hdef struct with vars
      hdef draw_hd;
      switch(dim){
        case 1: draw_hd.vars[0] = vars[0]; break;
        case 2: draw_hd.vars = {vars[0],vars[1],""}; break;
        case 3: draw_hd.vars = {vars[0],vars[1],vars[2]}; break;
        default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one...");
          break;
      }

      //for setting the histo name, get the node name or parse hist_name(<nbinsx>,<xlow>,<xhi>,<nbinsy>,<ylow>,<yhi>,<nbinsz>,<zlow>,<zhi>)
      auto name_and_binning = pos == std::string::npos ? hist.first : boost::algorithm::erase_head_copy(*drawcmd,pos+2);
      std::vector<std::string> namebinning;
      boost::algorithm::split(namebinning, name_and_binning , boost::algorithm::is_any_of("("));
      std::vector<std::string> binning;
      if(namebinning.size() > 1){//we have a binning
        boost::algorithm::trim_right_if(namebinning[1],boost::algorithm::is_any_of(")"));
        boost::algorithm::split(binning, namebinning[1],boost::algorithm::is_any_of(","));
        msgsvc.debugmsg("name binning "+namebinning[0]);
        for(const auto& b : binning)
          msgsvc.debugmsg("Binning: "+b);
      }
      //sanity check of the parsed binning
      bool use_std_binning = true;
      if(binning.size() > 0){
        if(3*dim != binning.size())
          msgsvc.warningmsg("Something seems to be wrong with the binning. Using standard binning instead...");
        else use_std_binning = false;
      }

      //fill the rest of hdef
      if(!use_std_binning){
        switch(dim){
          case 1:
            draw_hd.bins[0] = std::stoi(binning[0]);
            draw_hd.mins[0] = std::stod(binning[1]);
            draw_hd.maxs[0] = std::stod(binning[2]);
            break;
          case 2:
            draw_hd.bins = {std::stoi(binning[0]),std::stoi(binning[3]),128};
            draw_hd.mins = {std::stod(binning[1]),std::stod(binning[4]),0.};
            draw_hd.maxs = {std::stod(binning[2]),std::stod(binning[5]),0.};
            break;
          case 3:
            draw_hd.bins = {std::stoi(binning[0]),std::stoi(binning[3]),std::stoi(binning[6])};
            draw_hd.mins = {std::stod(binning[1]),std::stod(binning[4]),std::stod(binning[7])};
            draw_hd.maxs = {std::stod(binning[2]),std::stod(binning[5]),std::stod(binning[8])};
            break;
          default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one...");
            break;
        }
      }
      draw_hd.name = namebinning[0];
      draw_hd.wght = hist.second.get<std::string>("weight","");

      //call function that adds the current histogram to the augmented dataframe
      make_hist(std::move(dim),std::move(draw_hd),adf);

    }
    else if(const auto varx = hist.second.get_optional<std::string>("var")){
      //try to get other variables
      const auto vary = hist.second.get<std::string>("yvar","");
      const auto varz = hist.second.get<std::string>("zvar","");
      auto dim = 1u;
      if(!vary.empty()){
        dim++;
        if(!varz.empty())
          dim++;
      }

      hdef draw_hd;
      draw_hd.name = hist.second.get<std::string>("histname",hist.first);
      draw_hd.wght = hist.second.get<std::string>("weight","");
      draw_hd.vars = {*varx,vary,varz};
      draw_hd.bins = {hist.second.get<int>("nbins",128),hist.second.get<int>("nbinsy",128),hist.second.get<int>("nbinsz",128)};
      draw_hd.mins = {hist.second.get<double>("min",0.),hist.second.get<double>("ymin",0.),hist.second.get<double>("zmin",0.)};
      draw_hd.maxs = {hist.second.get<double>("max",0.),hist.second.get<double>("ymax",0.),hist.second.get<double>("zmax",0.)};

      //call function that adds the current histogram to the augmented dataframe
      make_hist(std::move(dim),std::move(draw_hd),adf);

    }
    else{
      msgsvc.errormsg("\"Draw\" or \"var\" key missing. Going to next histogram");
    }
  }

  //keep us entertained with a progress bar
  ProgressBar pg;
  pg.start(chain->GetEntries(),msgsvc,nSlots);
  std::mutex barMutex;
  unsigned long long evt = 0ull;
  auto loop_lambda = [&pg,&evt,&barMutex](unsigned int, auto& ) {
    std::lock_guard<std::mutex> l(barMutex); // lock_guard locks the mutex at construction, releases it at destruction
    pg.update(evt);
    evt++;
  };
  if(!lazy_1Dhistos.empty())
    lazy_1Dhistos[0].OnPartialResultSlot(1ull,loop_lambda);
  else if(!lazy_2Dhistos.empty())
    lazy_2Dhistos[0].OnPartialResultSlot(1ull,loop_lambda);
  else if(!lazy_3Dhistos.empty())
    lazy_3Dhistos[0].OnPartialResultSlot(1ull,loop_lambda);
  else
    msgsvc.errormsg("Nothing added. This should not happen");

  const auto oloc = wd + "/" + ofn;
  IOjuggler::dir_exists(oloc,msgsvc);
  TFile of(oloc.data(),configtree.get("outfopt","recreate").data());

  // We need to trigger the event loop with this dummy-action before trying to write to file.
  // Only seen when running with friend-files.
  // I could be a bug in root, but I don't have time to write a reproducer and error-report with this little information
  if(!lazy_1Dhistos.empty())
    lazy_1Dhistos[0]->GetDimension();
  else if(!lazy_2Dhistos.empty())
    lazy_2Dhistos[0]->GetDimension();
  else if(!lazy_3Dhistos.empty())
    lazy_3Dhistos[0]->GetDimension();
  else
    msgsvc.errormsg("Nothing added. This should not happen");
  of.cd();

  // Now the hists can be written to file
  for(auto lh : lazy_1Dhistos)
    lh->Write();
  for(auto lh : lazy_2Dhistos)
    lh->Write();
  for(auto lh : lazy_3Dhistos)
    lh->Write();
  of.Write();
  of.Close();

  const int time = pg.stop();
  msgsvc.infomsg("Elapsed time for event loop: "+std::to_string(time)+" s");

  return 0;
}
