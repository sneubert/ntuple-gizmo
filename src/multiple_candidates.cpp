/**
  @date:   2019-05-13
  @author: mstahl
  @brief:  Find multiple candidates and save them in a root tree
*/
//from ROOT
#include "TStopwatch.h"
#include "TH1I.h"
//local
#include <IOjuggler.h>
#include "ProgressBar.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

namespace pt = boost::property_tree;

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();

  //parse command line variables and put some of them in local variables
  auto options = IOjuggler::parse_options(argc, argv, "d:hi:o:t:v:w:","nonoptions: optional list of branches to copy to the output tree",0);
  MessageService msgsvc("multiple_candidates",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto wd   = options.get<std::string>("workdir");
  const auto ofn  = options.get<std::string>("outfilename");
  const auto tn   = options.get<std::string>("treename");
  const auto otn  = options.get<std::string>("wsname") == static_cast<std::string>("w") ? tn+"_Mult" : options.get<std::string>("wsname");

  //get the chain
  auto chain = IOjuggler::get_chain(tn,options.get<std::string>("infilename"),wd,msgsvc);

  //parse command line variables (non-options)
  std::vector<std::string> add_branches_out;
  if(auto eas = options.get_child_optional("extraargs"); (*eas).size() > 0){
    for(const auto& ea : *eas){//iterate non-options
      if(ea.second.data().find(";") != std::string::npos || ea.second.data().find(":") == std::string::npos){
        std::vector<std::string> current_branches_out;
        boost::algorithm::split(current_branches_out, ea.second.data(), boost::algorithm::is_any_of(";"));
        add_branches_out.insert(add_branches_out.end(), current_branches_out.begin(), current_branches_out.end());
      }
    }
    [[maybe_unused]] auto fes = IOjuggler::get_friends(options,chain,msgsvc);
  }
  const auto nbranches_out = add_branches_out.size();

  //get what we need from the chain by hand
  if(nbranches_out > 0){
    chain->SetBranchStatus("*",0);
    //we need the following branches no matter what
    chain->SetBranchStatus("runNumber", 1);
    chain->SetBranchStatus("eventNumber", 1);
    for(const auto& b : add_branches_out)
      chain->SetBranchStatus(b.data(),1);
  }
  unsigned int runNumber;
  unsigned long long eventNumber;
  chain->SetBranchAddress("runNumber",&runNumber);
  chain->SetBranchAddress("eventNumber",&eventNumber);

  //initialize vector of tree-index and unique event number and fill it by looping the input chain
  std::vector<std::pair<unsigned int,unsigned long long>> uevs;
  unsigned int numevents = chain->GetEntries();
  ProgressBar pg;
  pg.start(numevents,msgsvc);
  msgsvc.infomsg("Filling unique event number vector from Tree");
  for(unsigned int ie = 0; ie < numevents; ie++){
    chain->GetEntry(ie);
    pg.update(ie);
    uevs.emplace_back(ie,10e+9*runNumber+eventNumber);
  }
  pg.stop();
  pg.start(numevents,msgsvc);

  //Sort the vector of tree-index and unique event number by the event number
  msgsvc.infomsg("Sorting by unique event number");
  std::sort(uevs.begin(),uevs.end(),[](std::pair<unsigned int,unsigned long long> a,std::pair<unsigned int,unsigned long long> b){return a.second < b.second;});

  //Init output file and it's tree structure
  auto of = TFile::Open((wd+"/"+ofn).data(),"RECREATE");
  auto otree = chain->CopyTree("","",0);//we are calling a TTreePlayer function here https://root.cern.ch/doc/master/classTTreePlayer.html#a5da54b30770b4a8b25c54c6f8112efc9
  otree->SetName(otn.data());

  //Find multiple candidates and write them to the output tree
  pg.start(uevs.size(),msgsvc);
  unsigned long long previous_event = 0;
  unsigned int imult = 0u;
  unsigned short current_multiplicity = 1;
  const int nbins_mult_hist = 15;
  TH1I multiplicities("multiplicities",";Multiplicity;Candidates",nbins_mult_hist,1,nbins_mult_hist+1);
  bool write_previous = true;
  msgsvc.infomsg("Searching for and writing multiple candidates to "+wd+"/"+ofn);
  for(unsigned int ie = 0u; ie < uevs.size(); ie++){
    pg.update(ie);
    if(uevs[ie].second == previous_event){
      //we persist all solutions
      if(write_previous){
        chain->GetEntry(uevs[ie-1].first);
        if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG && imult < 20){
          chain->Show();
          msgsvc.debugmsg("Found multiple candidates: " + std::to_string(uevs[ie].second));
        }
        otree->Fill();
      }
      chain->GetEntry(uevs[ie].first);
      otree->Fill();
      imult++;
      //some bookkeeping
      current_multiplicity++;
      //the next 3 lines prevent writing candidates twice if more than 2 multiple candidates have been found
      write_previous = false;
    }
    else {
      write_previous = true;
      multiplicities.Fill(current_multiplicity);
      current_multiplicity = 1;
    }
    previous_event = uevs[ie].second;

    //some debug output
    if(ie < 20)
      msgsvc.debugmsg("Tree index "+std::to_string(uevs[ie].first)+", Unique event number: "+std::to_string(uevs[ie].second));
  }
  pg.stop();
  std::cout << std::endl;
  //Write tree and histo to the ouput file
  of->cd();
  otree->Write();
  multiplicities.Write();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG)
    otree->Print();

  //Print statistics
  msgsvc.infomsg(std::string(static_cast<int>(8*(nbins_mult_hist+3))+1, '*'));
  msgsvc.infomsg(TString::Format(TString::Format("%%-%ds*",static_cast<int>(8*(nbins_mult_hist+3))),
                                                 TString::Format("* Found %u multiple candidates in %u total events (%.2f %%)",
                                                                 imult,numevents,100*static_cast<float>(imult)/static_cast<float>(numevents)).Data()));
  msgsvc.infomsg(TString::Format(TString::Format("%%-%ds*",static_cast<int>(8*(nbins_mult_hist+3))),"* Multiplicities: "));
  TString line = "* Multiplicity  ";
  for(int j = 1; j < nbins_mult_hist+1; j++)
    line += TString::Format("%-8d",j);
  line += "overflow*";
  msgsvc.infomsg(line);
  line = "* Candidates    ";
  for(int j = 1; j < nbins_mult_hist+2; j++)
    line += TString::Format("%-8d",static_cast<int>(multiplicities.GetBinContent(j)));
  msgsvc.infomsg(line+"*");
  msgsvc.infomsg(std::string(static_cast<int>(8*(nbins_mult_hist+3))+1, '*'));

  //timing and cleanup
  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();
  of->Close();
  //let the garbage collection do the rest...
  return 0;
}
