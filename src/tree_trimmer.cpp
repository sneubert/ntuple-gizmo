/*!
  @brief: a simple TTree::CopyTree() would do the trick, but that doesn't work with friendtrees
**/

//C++
#include <type_traits>//decltype
#include <vector>
#include <exception>
#include <string>
#include <sstream>
//root
#include <TObject.h>
#include <TFile.h>
#include <TString.h>
#include <TRandom3.h>
#include <TTree.h>
#include <TLeaf.h>
#include <TBranch.h>
#include <TFriendElement.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TEntryList.h>
//local
#include <ProgressBar.h>
#include <IOjuggler.h>
#include "../cutter.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  auto options = IOjuggler::parse_options(argc, argv, "c:d:i:o:t:v:h","nonoptions: any number of <file(list):friendtree> combinations");
  MessageService msgsvc("tree_trimmer",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));

  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  auto wd = options.get<std::string>("workdir");
  auto chain = IOjuggler::get_chain(options.get<std::string>("treename"),options.get<std::string>("infilename"),wd,msgsvc);
  //add friend tree(s)
  [[maybe_unused]] auto fes = IOjuggler::get_friends(options,chain,msgsvc);

  //auto-append magic
  IOjuggler::auto_append_in_ptree(configtree);

  bool fullTree=!configtree.get_child_optional("variables");

  const bool noImplicitCuts= (const bool) (configtree.get_child_optional("noImplicitCuts"));

  msgsvc.infomsg("trimming input tree(s) with cut " + configtree.get<std::string>("basiccuts",""));
  if(fullTree)msgsvc.infomsg(" and writing out full tree");
  else msgsvc.infomsg(TString(" and writing out ") + configtree.get_child("variables").size() + " variables");

  if(noImplicitCuts) msgsvc.infomsg("Implicit cuts switched off");

  bool doSubsample = false;
  double subsamplefraction =1;
  int seed=6666;


  if(configtree.get_child_optional("subsample"))
  {
    doSubsample = true;
    subsamplefraction = configtree.get<double>("subsample.fraction",1);
    seed= configtree.get<double>("subsample.seed",6666);
    gRandom->SetSeed(seed);
    std::stringstream msg;
    msg<<std::setprecision(2) << "subsampling input tree(s) keeping " << subsamplefraction*100. << std::string("% of input events");
    msgsvc.infomsg(msg.str());
  }

  //apply cuts and save the indices of entries passing them
  chain->Draw(">>elist",configtree.get<std::string>("basiccuts","").data(),"entrylist");
  //get and loop this list
  TEntryList* list = static_cast<TEntryList*>(gDirectory->Get("elist"));
  chain->SetEntryList(list);

  chain->SetBranchStatus("*",0);

  //initialize leaves and outputs for filling the output tree
  TObjArray* listOfLeafs=chain->GetListOfLeaves();
  unsigned int nvars = listOfLeafs->GetEntries();
  if(!fullTree) nvars=static_cast<unsigned int>(configtree.get_child("variables").size());
  std::vector<TLeaf*> var_leaves(nvars,nullptr);
  std::vector<double> output_values(nvars,0.0);

  //create output file
  TFile of((wd + "/" + options.get<std::string>("outfilename")).data(),configtree.get("outfopt","recreate").data());
  std::string outtreename = configtree.get("outtreename",options.get<std::string>("treename"));
  TTree otree(outtreename.data(),outtreename.data());

  //initialise implicit cuts
  cutset implicit_cuts;
  //loop variables, do the branch logistics and the implicit cuts
  unsigned int var_counter = 0u;
  //save computing time by calling different loop if variables need to be transformed
  bool do_variable_transformation = false;
  std::vector<std::string> var_names(nvars);
  std::vector<std::string> new_var_names(nvars);
  if(!fullTree)
  {
    for(const auto& var : configtree.get_child("variables")){
      var_names[var_counter] = var.first;
      msgsvc.debugmsg("adding variable: " + var_names[var_counter]);
      chain->SetBranchStatus(var_names[var_counter].data(),1);
      var_leaves[var_counter] = chain->GetLeaf(var_names[var_counter].data());
      new_var_names[var_counter] = var.second.get("new_name",var_names[var_counter]);
      otree.Branch(new_var_names[var_counter].data(),&output_values[var_counter],(new_var_names[var_counter]+"/D").data());
      //check if new variables need to be parsed
      if(!do_variable_transformation && new_var_names[var_counter] != var_names[var_counter] &&
         (new_var_names[var_counter].find("log_") != std::string::npos || new_var_names[var_counter].find("sqrt_") != std::string::npos ||
          new_var_names[var_counter].find("atan_") != std::string::npos || new_var_names[var_counter].find("NNtfd_") != std::string::npos ))
        do_variable_transformation = true;
      //do implicit cuts
      if(!noImplicitCuts){
        if(!(var.second.get_optional<double>("low") && var.second.get_optional<double>("high")))
          throw std::runtime_error("mandatory range of variable not given");
        implicit_cuts.addCut(new range((var_names[var_counter] + "Range").data(),var_names[var_counter].data(),&output_values[var_counter],var.second.get<double>("low"),var.second.get<double>("high")));
      }
      var_counter++;
    }
  } // end variables defined in config file
  else { // coopy complete tree
    for(unsigned int var_counter=0;var_counter<nvars;++var_counter)
    {
      TLeaf* leaf=(TLeaf*)listOfLeafs->At(var_counter);
      var_names[var_counter]=leaf->GetName();
      msgsvc.debugmsg("adding variable: " + var_names[var_counter]);
      chain->SetBranchStatus(var_names[var_counter].data(),1);
      var_leaves[var_counter] = chain->GetLeaf(var_names[var_counter].data());
      new_var_names[var_counter] = leaf->GetName();
      otree.Branch(new_var_names[var_counter].data(),&output_values[var_counter],(new_var_names[var_counter]+"/D").data());
    }
  } // end get all variables


  //loop entry list and save the new tree
  auto nentries = list->GetN();
  ProgressBar pg;
  pg.start(nentries,msgsvc);
  if(do_variable_transformation){
    for (decltype(nentries) evt = 0; evt < nentries; evt++){
      //important: list->GetEntry(evt) returns the index of the passed event in the original tree
      chain->GetEntry(list->GetEntry(evt));
      pg.update(evt);
      // subsample if activated
      if(doSubsample && gRandom->Uniform()>subsamplefraction)continue;

      //fill the output tree
      for(typename std::decay<decltype(nvars)>::type ix = 0; ix < nvars; ix++)
        if(var_names[ix] != new_var_names[ix]){
          if(new_var_names[ix].find("log_") != std::string::npos)
            output_values[ix] = std::log10(static_cast<double>(var_leaves[ix]->GetValue(0)));
          else if(new_var_names[ix].find("sqrt_") != std::string::npos)
            output_values[ix] = std::sqrt(static_cast<double>(var_leaves[ix]->GetValue(0)));
          else if(new_var_names[ix].find("atan_") != std::string::npos)
            output_values[ix] = std::atan(static_cast<double>(var_leaves[ix]->GetValue(0)));
          else if(new_var_names[ix].find("NNtfd_") != std::string::npos)
            output_values[ix] = std::log10(static_cast<double>(var_leaves[ix]->GetValue(0))/(1-static_cast<double>(var_leaves[ix]->GetValue(0))));
          else
            output_values[ix] = static_cast<double>(var_leaves[ix]->GetValue(0));
        }
        else
          output_values[ix] = static_cast<double>(var_leaves[ix]->GetValue(0));
      if(noImplicitCuts || implicit_cuts.passall())
        otree.Fill();
    }
  }
  else{
    //loop without transformation
    for (decltype(nentries) evt = 0; evt < nentries; evt++){
      //important: list->GetEntry(evt) returns the index of the passed event in the original tree
      chain->GetEntry(list->GetEntry(evt));
      pg.update(evt);
      // subsample if activated
      if(doSubsample && gRandom->Uniform()>subsamplefraction)continue;

      //fill the output tree
      for(typename std::decay<decltype(nvars)>::type ix = 0; ix < nvars; ix++)
        output_values[ix] = static_cast<double>(var_leaves[ix]->GetValue(0));
      if(noImplicitCuts || implicit_cuts.passall())
        otree.Fill();
    }
  }
  const int time = pg.stop();
  msgsvc.infomsg("Elapsed time for trimming tree: "+std::to_string(time)+" s");
  otree.Write();
  of.Close();
  return 0;
}
