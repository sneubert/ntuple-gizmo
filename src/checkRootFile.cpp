//C++
#include <string>
//root
#include <TObject.h>
//local
#include <IOjuggler.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

// very simple tool to check if an object can be retrieved from a root file
// will throw if that does not work

int main(int argc, char** argv){

  auto options = IOjuggler::parse_options(argc, argv, "d:i:t:v:h");
  MessageService msgsvc("checkRootFile",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  auto wd = options.get<std::string>("workdir");
  auto fn = options.get<std::string>("infilename");
  auto tn = options.get<std::string>("treename");

  auto input = IOjuggler::get_file(fn,wd);
  auto obj = IOjuggler::get_obj<TObject>(input,tn);
  if(obj==nullptr) return 1; // IOjuggler::get_obj should have thrown by now if this is the case
  // but we have to use obj for something to pass the compiler warnings 
  return 0;
}
