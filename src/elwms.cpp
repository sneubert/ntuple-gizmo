//ROOT
#include "ROOT/RDataFrame.hxx"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TXMLEngine.h"
#include "TXMLDocument.h"
#include "TXMLNode.h"
//ROOFIT
#include "RooRealVar.h"
#include "RooArgList.h"
#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "RooKeysPdf.h"
#include "RooGaussian.h"
#include "RooFFTConvPdf.h"
//TMVA
#include "TMVA/Reader.h"
//C++
#include <string>
#include <vector>
#include <limits>
#include <cmath>
#include <algorithm>
//BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/range/iterator_range.hpp>
//IOjuggler
#include <IOjuggler.h>
#include <ProgressBar.h>
#include <misID_betaRDF.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

struct hdef {
  std::string               name;
  std::string               wght;
  std::array<std::string,3> vars;
  std::array<int,3>         bins;
  std::array<double,3>      mins;
  std::array<double,3>      maxs;
  //default ctor
  hdef(){
    name = "htemp";
    wght = "";
    vars = {"","",""};
    bins = {128,128,128};
    mins = {0.,0.,0.};
    maxs = {0.,0.,0.};
  }
};

namespace pt = boost::property_tree;

int main(int argc, char** argv){

  /////////////////////////////////////////////////////
  ///  parse command-line options, start stopwatch  ///
  /////////////////////////////////////////////////////
  TStopwatch clock;
  clock.Start();
  auto options = IOjuggler::parse_options(argc, argv, "c:d:i:m:o:r:t:w:hv:","nonoptions: any number of <file(list):friendtree> combinations");
  MessageService msgsvc("Eierlegende Wollmilchsau",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto wd  = options.get<std::string>("workdir");
  const auto ofn = options.get<std::string>("outfilename");
  const auto ifn = options.get<std::string>("infilename");
  const auto itn = options.get<std::string>("treename");

  ///////////////////////////////
  ///  runtime configuration  ///
  ///////////////////////////////
  pt::ptree configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //append and replace stuff in ptree
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);
  IOjuggler::SetRootVerbosity(static_cast<bool>(configtree.get_optional<int>("suppress_RootInfo")));

  ///////////////////////////////////////////////////////
  ///  get variables for storing and/or transforming  ///
  ///////////////////////////////////////////////////////
  const auto add_branch_mode = configtree.get<bool>("AddBranchMode",false);
  std::vector<std::string> vars_in = {};
  std::vector<std::string> vars_out = {};
  std::vector<std::pair<std::string,std::string>> trafos;
  //variables from config file
  if(const auto trim_vars = configtree.get_child_optional("variables")){
    if(add_branch_mode)
      msgsvc.infomsg("Adding " + std::to_string((*trim_vars).size()) + " input variable(s)");
    else
      msgsvc.infomsg("Running with " + std::to_string((*trim_vars).size()) + " input variable(s)");
    for(const auto& var : *trim_vars){
      if(var.first != "dummy" || var.first != "dummy_i")
        vars_in.push_back(var.first);
      if(auto nn = var.second.get_optional<std::string>("nn")){
        // decide whether to add the new brach to the output tree
        if(!var.second.get("to_output",1) || var.first == "dummy_i")
          msgsvc.debugmsg("Using " + *nn + " only as input branch");
        else
          vars_out.push_back(*nn);
        // register a transformation
        if(auto tfm = var.second.get_optional<std::string>("tf"))
          trafos.emplace_back(*nn,*tfm);
        else
          trafos.emplace_back(*nn,var.first);
      }
      //automatic transformation of array variables
      else if(var.first.find("[0]") != std::string::npos){
        vars_out.push_back(boost::algorithm::replace_all_copy(var.first, "[0]", "Z"));
        trafos.emplace_back(vars_out.back(),var.first);
      }
      else
        vars_out.push_back(var.first);
    }
  }
  else msgsvc.infomsg("Running with full set of input variables");

  //add command line variables
  if(auto eas = options.get_child_optional("extraargs"); (*eas).size() > 0)
    for(const auto& ea : *eas){//iterate non-options
      const auto split_pos = ea.second.data().find("->");
      if( split_pos != std::string::npos ){//was "->" found?
        const auto nn = ea.second.data().substr(0,split_pos);
        trafos.emplace_back(nn,ea.second.data().substr(split_pos+1));
        vars_out.push_back(nn);
      }
      else msgsvc.debugmsg("<variable->transformation> combination not found in "+ea.second.data());
    }

  //////////////////////////////////////////////
  ///  get input file(s) and make RDataFrame ///
  //////////////////////////////////////////////
  auto chain = IOjuggler::get_chain(itn,ifn,wd,msgsvc);
  //add friend tree(s).
  [[maybe_unused]] auto fes = IOjuggler::get_friends(options,chain,msgsvc);

  //first, we need to know how many events there are and how many threads we use:
  ROOT::EnableImplicitMT();//need to call this first to be able to call ROOT::GetImplicitMTPoolSize()
  auto nSlots   = ROOT::GetImplicitMTPoolSize();
  const auto nThreads = std::min(configtree.get("threads",nSlots),nSlots);
  if(nThreads == 0){
    ROOT::DisableImplicitMT();
    nSlots = 1;
    msgsvc.infomsg("Implicit multithreading is switched off");
  }
  else{
    ROOT::EnableImplicitMT(nThreads);
    msgsvc.infomsg("Running with " + std::to_string(nThreads) + " threads on "+ std::to_string(nSlots) + " slots");
  }

  ROOT::RDataFrame d(*chain.get());
  //use ROOT::RDF::RNode to chain commands on the dataframe
  ROOT::RDF::RNode df = d;

  /////////////////////////////
  ///  Add Lumi info tuple  ///
  /////////////////////////////
  if(configtree.get("LumiTuple",0)){
    try {
      msgsvc.debugmsg("Adding lumi info tuple");
      //this can throw
      auto lc = IOjuggler::get_chain(configtree.get("LumiTupleTree","GetIntegratedLuminosity/LumiTuple"),ifn,wd,msgsvc);
      ROOT::RDataFrame dlumi(*lc.get());
      const auto oloclumi = wd+"/forWs/Lumi_"+ofn;
      IOjuggler::dir_exists(oloclumi);

      const auto cols = dlumi.GetColumnNames();
      msgsvc.debugmsg("Column names: ");
      for(const auto& c : cols) msgsvc.debugmsg(c);
      //usually the following should work, but it doesn't:
      // if(dlumi.HasColumn("IntegratedLuminosity") && dlumi.HasColumn("IntegratedLuminosityErr"))
      if(std::find(cols.begin(), cols.end(), std::string("IntegratedLuminosity")) != cols.end() &&
         std::find(cols.begin(), cols.end(), std::string("IntegratedLuminosityErr")) != cols.end()){
        auto intlumi = dlumi.Sum("IntegratedLuminosity");
        //Lumi errors are simply added up,
        //see sl. 10 https://indico.cern.ch/event/100756/contributions/1296401/attachments/3262/4947/Lumi_Yas.pdf
        auto dintlumi = dlumi.Sum("IntegratedLuminosityErr");
        dlumi.Snapshot("LumiTuple", wd+"/forWs/Lumi_"+ofn,"");
        msgsvc.infomsg(TString::Format("Integrated luminosity: %.3f +- %.3f pb-1",intlumi.GetValue(),dintlumi.GetValue()));
      }
      else dlumi.Snapshot("LumiTuple", wd+"/forWs/Lumi_"+ofn,"");
    }
    catch(std::exception& e){
      msgsvc.warningmsg("Caught exception while trying to add LumiTuple: " + static_cast<std::string>(e.what()));
    }
  }

  ////////////////////////////////////////////////
  ///  Define actions (tansform/add variables) ///
  ////////////////////////////////////////////////
  if(const auto misID = configtree.get_child_optional("misIDMbeta")){
    std::vector<std::string> daughters;
    for(const auto& daughter : *misID)
      daughters.push_back(daughter.first);
    MbetaRDF(daughters,df,vars_out,msgsvc,configtree.get("add_lvs_to_output",false));
    msgsvc.debugmsg("Added masses and betas for misID studies");
  }
  //create index list
  if(configtree.get("create_indexlist",false)){
    std::atomic<int> idx(-1);//needed for thread-safety. there is a small portion of events which get's screwed up otherwise
    df = df.Define("index",[&idx] { return ++idx; });
    vars_out.push_back("index");
  }
  if(!trafos.empty())
    for(const auto& tf : trafos){
      msgsvc.debugmsg("transforming " + tf.second + " to a new branch: " + tf.first);
      df = df.Define(tf.first,tf.second);
    }

  ///////////////////
  ///  apply MVA  ///
  ///////////////////
  if(const auto wf = options.get<std::string>("weightfile");wf!=""){
    //parse multiple weight files and BDT response names from the command line
    std::vector<std::string> weightfile_response_combinations;
    boost::algorithm::split( weightfile_response_combinations, wf, boost::algorithm::is_any_of(";"));
    for(const auto& wfrc : weightfile_response_combinations){
      //split at last occurence of ':', so that weight files from xrootd locations can be added
      const auto split_pos = wfrc.find_last_of(':');
      if(split_pos != std::string::npos){//was ':' found at all?
        const auto rn  = wfrc.substr(split_pos+1);
        const auto wfn = wfrc.substr(0,split_pos);
        auto wloc = wd+"/"+wfn;//try this location
        if(gSystem->AccessPathName(wloc.data())){
          wloc = wfn;//try without wd
          if(gSystem->AccessPathName(wloc.data()))
            throw std::runtime_error("Cannot locate weightfile "+ wfn);
        }
        msgsvc.infomsg("Adding MVA response \""+rn+"\" from "+wloc);

        //parsing of xml file stolen from https://gitlab.cern.ch/lhcb/Phys/blob/master/Phys/MVADictTools/src/lib/TMVATransform.cpp
        TXMLEngine xmlparser;
        std::vector<std::string> MVAvars;
        std::string list_of_MVA_vars;
        // Loop over all children (nodes)
        auto child = xmlparser.GetChild(xmlparser.DocGetRootElement(xmlparser.ParseFile(wloc.data())));
        while(child != nullptr){
          if(auto nodename = static_cast<std::string>(xmlparser.GetNodeName(child)); nodename == "Variables"){
            auto gchild = xmlparser.GetChild(child);
            while(gchild != nullptr){
              MVAvars.emplace_back(xmlparser.GetAttr(gchild, "Expression"));
              list_of_MVA_vars += "static_cast<float>("+MVAvars.back()+"),";
              msgsvc.debugmsg("Training variable: "+MVAvars.back());
              gchild = xmlparser.GetNext(gchild);
            }
            break;//we don't need to iterate all the trees
          }
          child = xmlparser.GetNext(child);
        }//got variables from xml
        list_of_MVA_vars.pop_back();
        //check if they are known to our RDataFrame instance
        for(const auto& v : MVAvars)
          if(std::find(df.GetColumnNames().begin(), df.GetColumnNames().end(), v) == df.GetColumnNames().end())
            throw std::runtime_error("The variable "+v+" that has been declared in the MVA weight file "+wloc+" is not known by the current DataFrame");
        df = df.Define("rdf_vars_for"+rn,"std::vector<float>{"+list_of_MVA_vars+"};");

        //init Reader and do the Define magic
        std::vector<TMVA::Reader*> MVAreaders(nSlots,new TMVA::Reader(MVAvars,configtree.get("MVAReaderOpt","Silent")));
        for(auto sl = 0u; sl < nSlots; sl++)
          MVAreaders[sl]->BookMVA(rn+"_Reader_"+std::to_string(sl),wloc);//the question here is: does it take longer to read in the (quite large) xml files, or the evaluation of the response

        //capture everything by value here, otherwise bad alloc!
        //TODO: implement rarity
        df = df.DefineSlot(rn,[=](unsigned int sl, const std::vector<float>& cvar) -> double {
          return MVAreaders[sl]->EvaluateMVA(cvar,rn+"_Reader_"+std::to_string(sl));}, {"rdf_vars_for"+rn});
        vars_out.push_back(rn);
      }//end if ':' split was found
      else throw std::runtime_error("Could't parse <xml-file:MVA Response name> combination "+wf);
    }//end loop MVAs
  }

  ///////////////////////////////
  ///  Filter actions (cuts)  ///
  ///////////////////////////////
  if(const auto cut = configtree.get_optional<std::string>("Filter")){
    msgsvc.infomsg("Applying Filter action: " + *cut);
    df = df.Filter(*cut);
  }
  if(configtree.get("remove_nan_inf",false)){
    msgsvc.infomsg("Rejecting event if the value of one of the chosen varibles is NaN or inf");
    if(add_branch_mode || vars_out.empty())//TODO: implement if needed
      msgsvc.warningmsg("I can only apply nan or inf filter if \"variables\" node exists.");
    else {
      std::string ridiculous_hack = "";
      for(const auto& var : vars_out)
        ridiculous_hack += "!(std::isnan("+var+") || std::isinf("+var+")) && ";
      if(ridiculous_hack.size() > 4)
        ridiculous_hack.erase(ridiculous_hack.size() - 4);
      df = df.Filter(ridiculous_hack);
    }
  }

  //////////////////////
  ///  Progress bar  ///
  //////////////////////
  //lazy action that enables to draw the progress bar (the action doesn't take additional time)
  auto lac = df.Count();

  //keep us entertained with a progress bar
  ProgressBar pg;
  pg.start(chain->GetEntries(),msgsvc,nThreads);
  std::mutex barMutex;
  unsigned long long evt = 0ull;
  lac.OnPartialResultSlot(1ull, [&pg,&evt,&barMutex](unsigned int, auto& ) {
    std::lock_guard<std::mutex> l(barMutex); // lock_guard locks the mutex at construction, releases it at destruction
    pg.update(evt);
    evt++;
  });

  /////////////////////////
  ///  make histograms  ///
  /////////////////////////
  const auto hists = configtree.get_child_optional("hists");
  //make a vector of RResultPtr to histos (they trigger the event loop when Get is called on the object pointer,
  // but the RResultPtr is more than just a shared_ptr, which makes it un-castable to the TH1 base class. Therefore we have to explicitly use all three forms)
  std::vector<ROOT::RDF::RResultPtr<TH1D>> lazy_1Dhistos;
  std::vector<ROOT::RDF::RResultPtr<TH2D>> lazy_2Dhistos;
  std::vector<ROOT::RDF::RResultPtr<TH3D>> lazy_3Dhistos;
  if(hists){
    auto nh = (*hists).size();
    msgsvc.infomsg("Requested to produce " + std::to_string(nh) + " histograms");

    auto make_hist = [&lazy_1Dhistos,&lazy_2Dhistos,&lazy_3Dhistos,&msgsvc] (const auto&& dim, hdef&& hp, ROOT::RDF::RNode& adf) -> void {
      if(hp.wght.empty()){//it's not pretty like this, but should perform better than using a default weight of 1.
        switch(dim){
          case 1: lazy_1Dhistos.push_back(
                  adf.Histo1D(ROOT::RDF::TH1DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0]},hp.vars[0])
                );
            break;
          case 2: lazy_2Dhistos.push_back(
                  adf.Histo2D(ROOT::RDF::TH2DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0],
                                                   hp.bins[1],hp.mins[1],hp.maxs[1]},hp.vars[0],hp.vars[1])
                );
            break;
          case 3: lazy_3Dhistos.push_back(
                  adf.Histo3D(ROOT::RDF::TH3DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0],
                                                   hp.bins[1],hp.mins[1],hp.maxs[1],hp.bins[2],hp.mins[2],hp.maxs[2]},
                              hp.vars[0],hp.vars[1],hp.vars[2])
                );
            break;
          default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one...");
            break;
        }
      }
      else{
        switch(dim){
          case 1: lazy_1Dhistos.push_back(
                  adf.Histo1D(ROOT::RDF::TH1DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0]},hp.vars[0],hp.wght)
                );
            break;
          case 2: lazy_2Dhistos.push_back(
                  adf.Histo2D(ROOT::RDF::TH2DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0],
                                                   hp.bins[1],hp.mins[1],hp.maxs[1]},hp.vars[0],hp.vars[1],hp.wght)
                );
            break;
          case 3: lazy_3Dhistos.push_back(
                  adf.Histo3D(ROOT::RDF::TH3DModel{hp.name.data(),"",hp.bins[0],hp.mins[0],hp.maxs[0],
                                                   hp.bins[1],hp.mins[1],hp.maxs[1],hp.bins[2],hp.mins[2],hp.maxs[2]},
                              hp.vars[0],hp.vars[1],hp.vars[2],hp.wght)
                );
            break;
          default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one...");
            break;
        }
      }
    };

    for (const auto& hist : *hists){
      ROOT::RDF::RNode adf = df;
      //apply cuts
      if(const auto cut = hist.second.get_optional<std::string>("cut")){
        msgsvc.debugmsg("Applying Filter " + *cut);
        adf = adf.Filter(*cut);
      }
      //call histo lazy actions
      //decide whether to call Histo1D, Histo2D or Histo3D based on string parsing
      //we have to parse a Draw command here. do it like before, titles etc. can be passed later in draw_stuff
      if (const auto drawcmd = hist.second.get_optional<std::string>("Draw")){
        //a Draw command can look like: zvar:yvar:xvar>>hist_name(<nbinsx>,<xlow>,<xhi>,<nbinsy>,<ylow>,<yhi>,<nbinsz>,<zlow>,<zhi>) (note the order!)
        //strip everything right of >> to get the variables we want to plot
        auto pos = (*drawcmd).find(">>");//14 in the example string
        auto varsstr = pos == std::string::npos ? *drawcmd : boost::algorithm::erase_tail_copy(*drawcmd,(*drawcmd).size()-pos);//zvar:yvar:xvar
        std::vector<std::string> vars;
        boost::algorithm::split(vars, varsstr , boost::algorithm::is_any_of(":"));
        msgsvc.debugmsg("First variable of this \"Draw\" command "+vars[0]);
        const auto dim = vars.size();
        msgsvc.debugmsg("Drawing " + std::to_string(dim) + "D histogram");
        //start to fill hdef struct with vars
        hdef draw_hd;
        switch(dim){
          case 1: draw_hd.vars[0] = vars[0]; break;
          case 2: draw_hd.vars = {vars[0],vars[1],""}; break;
          case 3: draw_hd.vars = {vars[0],vars[1],vars[2]}; break;
          default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one...");
            break;
        }

        //for setting the histo name, get the node name or parse hist_name(<nbinsx>,<xlow>,<xhi>,<nbinsy>,<ylow>,<yhi>,<nbinsz>,<zlow>,<zhi>)
        auto name_and_binning = pos == std::string::npos ? hist.first : boost::algorithm::erase_head_copy(*drawcmd,pos+2);
        std::vector<std::string> namebinning;
        boost::algorithm::split(namebinning, name_and_binning , boost::algorithm::is_any_of("("));
        std::vector<std::string> binning;
        if(namebinning.size() > 1){//we have a binning
          boost::algorithm::trim_right_if(namebinning[1],boost::algorithm::is_any_of(")"));
          boost::algorithm::split(binning, namebinning[1],boost::algorithm::is_any_of(","));
          msgsvc.debugmsg("name binning "+namebinning[0]);
          for(const auto& b : binning)
            msgsvc.debugmsg("Binning: "+b);
        }
        //sanity check of the parsed binning
        bool use_std_binning = true;
        if(binning.size() > 0){
          if(3*dim != binning.size())
            msgsvc.warningmsg("Something seems to be wrong with the binning. Using standard binning instead...");
          else use_std_binning = false;
        }

        //fill the rest of hdef
        if(!use_std_binning){
          switch(dim){
            case 1:
              draw_hd.bins[0] = std::stoi(binning[0]);
              draw_hd.mins[0] = std::stod(binning[1]);
              draw_hd.maxs[0] = std::stod(binning[2]);
              break;
            case 2:
              draw_hd.bins = {std::stoi(binning[0]),std::stoi(binning[3]),128};
              draw_hd.mins = {std::stod(binning[1]),std::stod(binning[4]),0.};
              draw_hd.maxs = {std::stod(binning[2]),std::stod(binning[5]),0.};
              break;
            case 3:
              draw_hd.bins = {std::stoi(binning[0]),std::stoi(binning[3]),std::stoi(binning[6])};
              draw_hd.mins = {std::stod(binning[1]),std::stod(binning[4]),std::stod(binning[7])};
              draw_hd.maxs = {std::stod(binning[2]),std::stod(binning[5]),std::stod(binning[8])};
              break;
            default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one...");
              break;
          }
        }
        draw_hd.name = namebinning[0];
        draw_hd.wght = hist.second.get<std::string>("weight","");

        //call function that adds the current histogram to the augmented dataframe
        make_hist(std::move(dim),std::move(draw_hd),adf);

      }
      else if(const auto varx = hist.second.get_optional<std::string>("var")){
        //try to get other variables
        const auto vary = hist.second.get<std::string>("yvar","");
        const auto varz = hist.second.get<std::string>("zvar","");
        auto dim = 1u;
        if(!vary.empty()){
          dim++;
          if(!varz.empty())
            dim++;
        }

        hdef draw_hd;
        draw_hd.name = hist.second.get<std::string>("histname",hist.first);
        draw_hd.wght = hist.second.get<std::string>("weight","");
        draw_hd.vars = {*varx,vary,varz};
        draw_hd.bins = {hist.second.get<int>("nbins",128),hist.second.get<int>("nbinsy",128),hist.second.get<int>("nbinsz",128)};
        draw_hd.mins = {hist.second.get<double>("min",0.),hist.second.get<double>("ymin",0.),hist.second.get<double>("zmin",0.)};
        draw_hd.maxs = {hist.second.get<double>("max",0.),hist.second.get<double>("ymax",0.),hist.second.get<double>("zmax",0.)};

        //call function that adds the current histogram to the augmented dataframe
        make_hist(std::move(dim),std::move(draw_hd),adf);

      }//end drawing with var or Draw commands
      else msgsvc.errormsg("\"Draw\" or \"var\" key missing. Going to next histogram");
    }//end looping hists
  }

  ///////////////////////////////////////////
  ///  Snapshot action (saving the file)  ///
  ///////////////////////////////////////////
  //check here if we want to save a RooDataSet to make the Snapshot lazy
  const auto wsv = configtree.get_child_optional("ws_vars");
  //options for the output file the following options are currently (Dec 18) being updated by the root developers.
  //Check some time later here: https://root.cern.ch/doc/master/Compression_8h_source.html
  ROOT::RDF::RSnapshotOptions rso(configtree.get("outfopt","RECREATE"),
                                  static_cast<ROOT::ECompressionAlgorithm>(configtree.get<unsigned int>("outcompalg",ROOT::RCompressionSetting::EAlgorithm::kInherit)),
                                  configtree.get<unsigned int>("outcomplvl",ROOT::RCompressionSetting::ELevel::kInherit),0,99,static_cast<bool>(wsv));//wsv: make snapshot lazy if we save a ws

  //for lazy snapshots. see https://root-forum.cern.ch/t/rdataframe-iterative-snapshots/32732/8
  using SnapRet_t = ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager>>;
  std::vector<SnapRet_t> rets;
  const auto oloc = wd+"/forWs/"+ofn;
  IOjuggler::dir_exists(oloc,msgsvc);
  if(add_branch_mode || vars_out.empty())
    rets.emplace_back(df.Snapshot(configtree.get("outtreename",itn),oloc,"",rso));
  else
    rets.emplace_back(df.Snapshot(configtree.get("outtreename",itn),oloc,vars_out,rso));

  //////////////////////////////////////////////
  ///  get variables to put in RooWorkspace  ///
  //////////////////////////////////////////////
  if(wsv){
    //we need this vector so that the RooRealVars don't go out of scope
    std::vector<std::vector<std::unique_ptr<RooRealVar>>> variables(nSlots);
    std::vector<RooArgList> alists(nSlots);
    std::string list_of_ws_vars;
    const auto dsname = options.get<std::string>("dsname");
    std::vector<RooDataSet> dsets;
    for(typename std::decay<decltype(nSlots)>::type sl = 0; sl < nSlots; sl++){
      for(const auto& v : *wsv){
        if(const auto name = v.first; std::find(vars_out.begin(), vars_out.end(), name) != vars_out.end()){
          list_of_ws_vars += "static_cast<double>("+name+"),";
          std::unique_ptr<RooRealVar> myvar(new RooRealVar(name.data(),name.data(),
                                                           v.second.get("low",-std::numeric_limits<double>::infinity()),
                                                           v.second.get("high",std::numeric_limits<double>::infinity())));
          if(const auto no_duplicate = alists[sl].add(*myvar.get()); no_duplicate)
            variables[sl].push_back(std::move(myvar));
        }
        else msgsvc.warningmsg("Could not add "+name+" to RooDataset, since it is not in output variables, defined by the \"variables\" node");
      }
      //we have to do this awkward exception here, because renaming the RooDataSet during import kills the links to the RooRealVars and nothing will happen when you try to fit something...
      if(sl == 0)
        dsets.emplace_back((dsname).data(),(dsname).data(),alists[sl]);
      else
        dsets.emplace_back((dsname+std::to_string(sl)).data(),(dsname+std::to_string(sl)).data(),alists[sl]);
    }
    list_of_ws_vars.pop_back();
    df = df.Define("rdf_vars_for_ws","std::vector<double>{"+list_of_ws_vars+"};");

    //////////////////////////////////
    ///  fill and save RooDataSet  ///
    //////////////////////////////////
    const auto n_ws_vars = alists[0].getSize();
    auto save_in_ds = [&alists,&dsets,&n_ws_vars] (unsigned int thread, const std::vector<double>& rdf_vars) -> void {
      for(auto i = 0u; i < n_ws_vars; i++)
        static_cast<RooRealVar*>(alists[thread].at(i))->setVal(rdf_vars[i]);
      dsets[thread].add(alists[thread]);
    };
    df.ForeachSlot(save_in_ds,{"rdf_vars_for_ws"});
    std::cout << std::endl;
    for(auto& tds : boost::make_iterator_range(dsets.begin()+1, dsets.end()))
      dsets[0].append(tds);
    const auto wsoloc = wd+"/Ws/"+ofn;//HARDCODED
    IOjuggler::dir_exists(wsoloc,msgsvc);
    auto wsf = TFile::Open(wsoloc.data(),configtree.get("outfopt","recreate").data());
    RooWorkspace w(options.get<std::string>("wsname").data(),true);
    w.import(dsets[0]);

    /////////////////////////
    ///  create KDE pdfs  ///
    /////////////////////////
    if(configtree.get_child_optional("kdepdfs")){
      for(const auto& v : configtree.get_child("kdepdfs")){
        //get shapes from external files - access the file and tree
        const auto kdeifn = v.second.get<std::string>("filename");
        const auto kdeitn = v.second.get<std::string>("treepath");
        const auto kdeobs = static_cast<std::string>(v.second.get("obsinfile",(*wsv).front().first));
        auto MCt = IOjuggler::get_obj<TTree>(IOjuggler::get_file(kdeifn,wd),kdeitn);
        //load data from tree manually, since using import is in general not possible.
        //This is because the variable in MCtree should have the same name as the fit-observable
        auto MCl = MCt->GetLeaf(kdeobs.data());

        const auto obsname = v.second.get_optional<std::string>("observable");
        auto KDEObservable = obsname ? static_cast<RooRealVar*>(alists[0].find((*obsname).data())) : static_cast<RooRealVar*>(alists[0].find((*wsv).front().first.data()));
        if(KDEObservable == nullptr){
          msgsvc.errormsg("Trying to create KDE p.d.f., but cannot find " + *obsname + ". Or the Observable parsed from the \"observable\" node is nullptr...");
          return 1;
        }
        msgsvc.infomsg("Creating KDE p.d.f.");
        msgsvc.infomsg(TString::Format("  %10s: %s","filename",kdeifn.data()));
        msgsvc.infomsg(TString::Format("  %10s: %s","treename",kdeitn.data()));
        msgsvc.infomsg(TString::Format("  %10s: %s","variable",kdeobs.data()));
        msgsvc.infomsg(TString::Format("  %10s: %s","observable",KDEObservable->GetName()));

        RooDataSet MCds("MCds","MCds",*KDEObservable);
        auto nEntriesMC = MCt->GetEntries();
        //for the resolution function's <Q> (see below)
        auto meanQ = 0.f; unsigned int acc = 0u;
        for (decltype(nEntriesMC) MCev = 0; MCev < nEntriesMC; MCev++){
          MCt->GetEntry(MCev);
          KDEObservable->setVal(MCl->GetValue(0));
          if(KDEObservable->getMin() < KDEObservable->getVal() && KDEObservable->getVal() < KDEObservable->getMax()){
            MCds.add(*KDEObservable);
            meanQ += KDEObservable->getVal();
            acc++;
          }
        }

        //make KDE-pdf from MC data
        RooKeysPdf kdepdf(v.first.c_str(),v.first.c_str(),*KDEObservable,MCds,
                          static_cast<RooKeysPdf::Mirror>(v.second.get("mirror",3)),
                          static_cast<double>(v.second.get("rho",1.f))
                          );

        //fold it with the detector resolution (optional)
        //it's not possible to directly fold a resolution function with mass dependent width https://root.cern.ch/phpBB3/viewtopic.php?t=14482
        //we keep the resolution fixed at sqrt(<Q>), where <Q> is the energy release of the sample mean of our keys pdf
        if (configtree.get_child_optional("resolution") && static_cast<bool>(v.second.get("fold",1))){
          meanQ /= static_cast<decltype(meanQ)>(acc);
          meanQ -= configtree.get("resolution.MassThreshold",0.f);
          auto resolution = sqrt(meanQ)*configtree.get("resolution.ResolutionScaling",1.f);
          msgsvc.infomsg(TString::Format("Folding %s with a %.2f MeV Gaussian resolution",kdepdf.GetName(),resolution));
          RooRealVar mg(TString::Format("%smg",kdepdf.GetName()).Data(),"mg",0);
          RooRealVar sg(TString::Format("%ssg",kdepdf.GetName()).Data(),"sg",resolution);
          RooGaussian gauss(TString::Format("%sres",kdepdf.GetName()).Data(),"Gaussian resolution",*KDEObservable,mg,sg);
          RooFFTConvPdf kxg(TString::Format("%sxg",kdepdf.GetName()).Data(),"kde (X) gauss",*KDEObservable,kdepdf,gauss);
          //w makes copy when importing. so we are safe when importing it in this scope and writing it later to file
          w.import(kxg);
        }
        else
          w.import(kdepdf);
      }
    }

    if(msgsvc.GetMsgLvl() > MSG_LVL::INFO)
      w.Print();
    w.Write();
    wsf->Close();
  }// end putting stuff into RooWorkspace
  else std::cout << std::endl; //clear line after event loop

  //write lazy hists to file. The event loop should have run at this point
  if(hists){
    const auto histoloc = wd+"/hists/"+ofn;//HARDCODED
    IOjuggler::dir_exists(histoloc,msgsvc);
    auto histf = TFile::Open(histoloc.data(),configtree.get("outfopt","recreate").data());
    for(auto lh : lazy_1Dhistos)
      lh->Write();
    for(auto lh : lazy_2Dhistos)
      lh->Write();
    for(auto lh : lazy_3Dhistos)
      lh->Write();
    histf->Close();
  }
  const int time = pg.stop();
  msgsvc.infomsg("Elapsed time for event loop: "+std::to_string(time)+" s");

  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO) clock.Print();

  return 0;
}
