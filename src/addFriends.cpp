//C++
#include <type_traits>
#include <vector>
#include <map>
#include <exception>
#include <string>
#include <sstream>
#include <functional>
#include <chrono>
#include <thread>
//root
#include <TROOT.h>
#include <TObject.h>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <TLeaf.h>
#include <TBranch.h>
#include <TFriendElement.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TEntryList.h>
#include <TEnv.h>
#include <TStopwatch.h>
//new RDF-stuff
#include <ROOT/RDataFrame.hxx>
#include <ROOT/RDF/RInterface.hxx>
//local
#include "ProgressBar.h"
#include "IOjuggler.h"
#include "misID_betaRDF.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();

  //parse command line options
  const auto options = IOjuggler::parse_options(argc, argv, "c:i:t:d:o:v:h");
  const auto wd = options.get<std::string>("workdir");
  const auto ofn = options.get<std::string>("outfilename");
  const auto ifn = options.get<std::string>("infilename");
  const auto itn = options.get<std::string>("treename");

  //init MessageService for easy and nice printout
  MessageService msgsvc("addFriends",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));

  //get config-file
  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
 
  //Prepare main TTree
  auto fmain=IOjuggler::get_file(ifn, wd);
  auto tmain=IOjuggler::get_obj<TTree>(fmain, itn);

  //get friends  
  const auto friendfiles = configtree.get_child_optional("friendfiles");
  
  msgsvc.infomsg("Adding " + std::to_string((*friendfiles).size()) + " friends");
  

  //add friends 
  std::vector< TTree* > fes;
  
  for(const auto& ff : configtree.get_child("friendfiles")){
    
    auto tfriend=IOjuggler::get_obj<TTree>(IOjuggler::get_file(ff.second.get<std::string>("file"), wd), ff.second.get<std::string>("tree"));
    tmain->AddFriend(tfriend);
    
  }
  
  //Create RDF snapshot
  ROOT::RDataFrame d(*tmain);
  auto f = d.Snapshot(itn, wd + "/" +ofn);

  return 0;

}
