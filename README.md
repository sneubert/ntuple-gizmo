# Please contribute
This is open analysis code. All LHCb members are invited to contribute. Please have a look at our [guidelines](CONTRIBUTING.md) for contributions.

# Documentation
The package contains several wrapper scripts that allow using ROOT by writing config files only
(based on the INFO parser of [boost::property_tree](http://www.boost.org/doc/libs/1_64_0/doc/html/property_tree/parsers.html#property_tree.parsers.info_parser)) <br>
Links to the documentation of the wrappers:
- [stuff2hist](doc/stuff2hist.md) for making ROOT histograms from TTrees, TChains or RooDataSets
- [draw_stuff](doc/draw_stuff.md) for plotting with ROOT
- [tree_trimmer](doc/tree_trimmer.md) for TTree manipulations
- [misID_beta](doc/misID_beta.md) for studying misidentification backgrounds
- [CutOptScan](doc/CutOptScan.md) for rectangular cut optimisation
- [trainMVA](doc/trainMVA.md) for classification or regression tasks with TMVA
- [applyMVA](doc/applyMVA.md) for applying TMVA weight-files to ntuples
- [tree_trimmerRDF](doc/tree_trimmerRDF.md) for trimming trees and chains
- [elwms](doc/elwms.md) for trimming trees and chains, applying MVAs, writing RooWorkspaces and making histograms all at the same time
- [multiple_candidates](doc/multiple_candidates.md) for finding multiple candidates and persisting them in an output tree

# Build standalone package
The initial step is to check out the repository, navigate there and update the IOjuggler submodule:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/sneubert/ntuple-gizmo.git  (our your own fork)
cd ntuple-gizmo
git submodule update --init --recursive
```
To build the package (e.g. on lxplus), you need to setup a suiting environment.<br>
If you are lucky, `cvmfs` is mounted on the machine you are working on, 
and you can use the LCG views like 
```
source /cvmfs/sft.cern.ch/lcg/views/LCG_latestpython3/x86_64-centos7-gcc8-opt/setup.sh
```
or the `setup.sh` script (which will do something equivalent) like this
```
source setup.sh [<LCG-version>] [<platform>]
```
Both arguments are optional and default to `95` and `x86_64-slc6-gcc8-opt` respectively.<br>
You can get the env from the views above with
```
source setup.sh latestpython3 x86_64-centos7-gcc8-opt
```
Now, the package can be build with
```
mkdir build; cd build; cmake ..; make; cd -
```
You may also want to be able to enable [`TH2A`](https://gitlab.cern.ch/mstahl/2DAdaptiveBinning) support by doing `cmake -DWITH_TH2A_PLOTTING=ON ..`.
Make sure cmake has not cached this option when switching this flag on and off.

# Integrate package into your project
You can integrate the package as a submodule into your package. Your parent `CMakeLists.txt` must only setup ROOT with RooFit TMVA and EG, boost and include the `src` directory of this package
as for example this [CMakeLists](CMakeLists.txt), or the [DfromB CMakeLists](https://gitlab.cern.ch/sneubert/DfromBBDTs/blob/master/CMakeLists.txt)
