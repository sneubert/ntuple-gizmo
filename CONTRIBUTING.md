This analysis code is open source,
and we welcome contributions of all kinds:
new tools,
fixes to existing code,
bug reports,
and reviews of proposed changes are all equally welcome.

By contributing,
you are agreeing that we may redistribute your work under
You also agree to abide by our
[contributor code of conduct](CONDUCT.md).

## Getting Started

1.  We use the [fork and merge](https://about.gitlab.com/2014/09/29/gitlab-flow/) model to manage changes. More information
    about [forking a repository](https://gitlab.cern.ch/help/workflow/forking_workflow.md#creating-a-fork) and [making a Merge Request](https://gitlab.cern.ch/help/workflow/forking_workflow.md#merging-upstream).

2.  To run the analysis code please install the [dependencies](#DEPENDENCIES).

3.  For our code,
    you should branch from and submit merge requests against the `master` branch.

## Dependencies


