#ifndef SELECTION_DECAYANGLES_H
#define SELECTION_DECAYANGLES_H 1

#include <TLorentzVector.h>
#include <TVector3.h>

//compute the phi angles between decay planes
//check in DaVinci/Analysis/Phys/DaVinciAngleCalculators for reference
double Phi_AngleDecayPlanes(const TLorentzVector qvect_A, const TLorentzVector qvect_B,
                            const TLorentzVector qvect_b1, const TLorentzVector qvect_b2){
  
  //mother --> A + B, B --> b1 + b2
  //everything is boosted to the rest frame of the decaying mother
  
  //build the mother q-vector
  TLorentzVector qvect_mother = qvect_A + qvect_B;
  
  //compute the boost to the mother rest frame
  TVector3 mother_boost = -qvect_mother.BoostVector();
  
  //boost all the particles in the mother rest frame
  TLorentzVector qvect_A_boosted = qvect_A;
  TLorentzVector qvect_B_boosted = qvect_B;
  TLorentzVector qvect_b1_boosted = qvect_b1;
  TLorentzVector qvect_b2_boosted = qvect_b2;
  
  qvect_A_boosted.Boost(mother_boost);
  qvect_B_boosted.Boost(mother_boost);
  qvect_b1_boosted.Boost(mother_boost);
  qvect_b2_boosted.Boost(mother_boost);
  
  //compute the directions of the particles in the mother rest frame
  TVector3 dir_A_boosted = (qvect_A_boosted.Vect()).Unit();
  TVector3 dir_B_boosted = (qvect_B_boosted.Vect()).Unit();
  TVector3 dir_b1_boosted = (qvect_b1_boosted.Vect()).Unit();
  TVector3 dir_b2_boosted = (qvect_b2_boosted.Vect()).Unit();
  
  //normal directions to the mother --> A + B and B --> b1 + b2 decay planes
  TVector3 el = (dir_A_boosted.Cross(dir_B_boosted)).Unit();
  TVector3 ek = (dir_b1_boosted.Cross(dir_b2_boosted)).Unit();
  
  //B direction in the mother frame
  //TVector3 ez = Bdec_axis_boosted.Unit();
  TVector3 ez = dir_B_boosted;
  
  //finally compute the phi angle!
  double cosPhi = ek.Dot(el);
  double sinPhi = (el.Cross(ek)).Dot(ez);
  double phi    = acos(cosPhi);
  
  //resolve the angle ambiguity
  if(sinPhi > 0.)
    return phi;
  else
    return (-phi);
}

#endif // SELECTION_DECAYANGLES_H
