
#ifndef INCLUDE_ENVPARSER_H
#define INCLUDE_ENVPARSER_H 1

/*!
 *  @file      commonLib.h
 *  @author    Alessio Piucci
 *  @brief     Common library of methods used by different macros and structures.
 */

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

using namespace std;

//parse a string containing some environment variables
inline std::string ParseEnvName(std::string input_string){

  //I first search the env variable in the input string
  size_t start_pos = input_string.find("$");

  if(start_pos == std::string::npos)
    return input_string; //if no $ are find, there are no env variables to convert
  else{
    std::string env_variable = input_string.substr(start_pos, input_string.find("/", start_pos + 1));

    //to convert the env variable, I have to skip the first $ char
    char *env_variable_converted = getenv((env_variable.substr(1, env_variable.size())).c_str());

    std::cout << "Converted " << env_variable << " --> " << env_variable_converted << std::endl;

    input_string.replace(start_pos, input_string.find("/", start_pos + 1), env_variable_converted);

    //now search for the next occurance of an env variable
    return ParseEnvName(input_string);
  }
}

#endif // INCLUDE_ENVPARSER_H
