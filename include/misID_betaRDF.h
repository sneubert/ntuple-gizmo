#ifndef MISID_BETARDF_H
#define MISID_BETARDF_H

#include <vector>
#include <string>
#include <type_traits>
#include <functional>
#include <algorithm>

#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "../IOjuggler/MessageService.h"

typedef std::vector<unsigned int> uivec;

template <typename RDF>
void MbetaRDF(std::vector<std::string>& daughters, RDF& df, std::vector<std::string>& ob_names,
              const MessageService& msgsvc = MessageService("MbetaRDF",MSG_LVL::INFO), const bool& add_lvs = false) {

  unsigned int n_daughters = daughters.size();
  msgsvc.infomsg("Setting up M-beta combinations for " + std::to_string(n_daughters) + " daughters");

  //get the daughter columns to define the TLorentzVectors
  std::string list_of_4vectors;
  for(const auto& d : daughters){
    const auto lv_name = d+"_lv";
    //add the lorentz vectors to the current dataframe
    df = df.Define(lv_name,[](double& px, double& py, double& pz, double& pe){return TLorentzVector(px,py,pz,pe);},{d+"_PX",d+"_PY",d+"_PZ",d+"_PE"});
    //add them to the list of output branches if desired
    if(add_lvs) ob_names.push_back(lv_name);
    list_of_4vectors += lv_name+",";
  }
  //df.BuildJittedNodes();
  list_of_4vectors.pop_back();
  msgsvc.debugmsg("This list of 4-vectors is being pushed into the data frame: " + list_of_4vectors);
  //we now have branches called <daughtername>_lv in our output tree, which contain the corresponding TLorentzVector
  df = df.Define("misID_vlv","std::vector<TLorentzVector>{"+list_of_4vectors+"};");
  // two things to note here: a) can't use the Define call with a cpp lambda, because it would need a variadic function call which is not suported, see: https://root-forum.cern.ch/t/rdataframe-define-with-variadic-function/29368
  // b) don't push this auxillary vector into the vector of output columns (ob_names)
  msgsvc.debugmsg("Managed to define the vector of Lorentz vectors. Doing the combinatorics now...");

  //construct all possible n_body combinations given n_daughters
  //the idea to construct the combinations is that the combination indices are never the same number
  //and rise with the index number. to achieve this, the last index gets incremented until
  //n_daughters is reached, then the second to last is incremented, while building all possible
  //combinations with the last index etc. this makes the function recursive.
  //EXAMPLE (3 body combinations for 5 final state daughters)
  // 123, 124, 125, 134, 135, 145, 234, 235, 245, 345
  std::function<void(uivec&, unsigned int)> combinatorics = [&n_daughters,&df,&combinatorics,&ob_names] (uivec& combination_indices, unsigned int index) -> void {

    //could also pass this parameter as const unsigned int (=n_body from outside scope)
    auto n_body_comb = combination_indices.size();

    //add functor for betas. we need to pass the indices in this way. it doesn't work by capturing...
    auto beta_functor = [](std::vector<TLorentzVector>& lvs, const uivec& cis) {
      double p0 = lvs[cis[0]-1].P(), beta_stub = 0;
      for(unsigned int i = 1; i < cis.size(); i++)
        beta_stub += lvs[cis[i]-1].P();
      return (beta_stub-p0)/(beta_stub+p0);
    };

    //add functor for invariant masses
    auto mass_functor = [](std::vector<TLorentzVector>& lvs, const uivec& cis) {
      TLorentzVector temp;
      for(const auto& idx : cis)
        temp += lvs[idx-1];
      return temp.M();
    };

    //function to call Define
    auto call_define = [&df,&beta_functor,&mass_functor,&ob_names,&combination_indices] (std::string&& name) -> void {
      std::string indices_as_string = "";
      for(const auto& idx : combination_indices){
        name += std::to_string(idx);
        indices_as_string += std::to_string(idx) + ",";
      }
      indices_as_string.pop_back();
      //capturing the indices in the mass and beta functors doesn't work. i have no clue why. but this implementation here does the job:
      df = df.Define("indices"+name,"std::vector<unsigned int>{"+indices_as_string+"};");
      if(name[0] == 'M')
        df = df.Define(name,mass_functor,{"misID_vlv","indices"+name});
      else
        df = df.Define(name,beta_functor,{"misID_vlv","indices"+name});
      ob_names.push_back(name);
    };

    //simple function to get all relevant permutations for beta of (n>3)-body combinations
    auto permutations = [&call_define,&combination_indices] () -> void {
      for(unsigned int i = 0; i < combination_indices.size(); i++){
        call_define("beta_");
        //move first element to the end (just convention to write the rotation afterwards.)
        //http://en.cppreference.com/w/cpp/algorithm/rotate
        std::rotate(combination_indices.begin(),combination_indices.begin()+1,combination_indices.end());
      }
    };

    //break condition ultimately fulfilled when function tries
    //to increment the first index to an invalid value
    while(combination_indices[index] <= n_daughters - n_body_comb + index + 1){

      if(index > 0 && combination_indices[index] <= combination_indices[index-1])
        combination_indices[index] = combination_indices[index-1]+1;

      if(index < n_body_comb-1)
        combinatorics(combination_indices,index+1);
      else if(index == n_body_comb-1){
        call_define("M_");
        if(n_body_comb > 2)
          permutations();
        else
          call_define("beta_");
      }
      else throw std::runtime_error("How the hell did i end up here?");

      //count up here. reset to lowest possible values before combination index would exceed maximum
      //at_max is a measure for indices being at their maximal value. if this condition is not met,
      //but the current index is at max, this index and all following are set back to their minimally
      //possible value. this value is dictated by the previous index
      bool at_max = true;
      for(unsigned int i = 0; i < combination_indices.size(); i++)
        if(combination_indices[i] < n_daughters - n_body_comb + i + 1)
          at_max = false;
      if(combination_indices[index] == n_daughters - n_body_comb + index + 1 && !at_max && index > 0){
        for(unsigned int i = index; i < combination_indices.size(); i++)
          combination_indices[i] = combination_indices[i-1] + 1;
        break;
      }
      else
        combination_indices[index] += 1;
    }
  };

  //iterate all possible n_body combinations
  for(auto n_body = n_daughters; n_body > 1; n_body--){//go from full n-body down to two-body combinations
    uivec combination_indices(n_body,0);//init index vector
    for(decltype(combination_indices.size()) i = 0; i < combination_indices.size(); i++)
      combination_indices[i] = i+1;//fill index vector with ascending values
    combinatorics(combination_indices,0u);
  }
  return;
}
#endif
