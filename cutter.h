#ifndef SELECTION_CUTTER_H 
#define SELECTION_CUTTER_H 1


#include <stdio.h>
#include <stdlib.h>
#include<iostream>
#include<fstream>
#include<sstream>
#include<functional>
#include<vector>
#include<TH1D.h>
#include<type_traits>

typedef std::less<double> lss;
typedef std::greater<double> grt; 

// operator functors
// the stl defines
// std::less
// std::greater
//using std::less;
//using std::greater;

// interface
class IEval 
{
public: 
  std::string name;     //name of the cut
  std::string varname;  //name of the variable used for the cut
  
  virtual ~IEval() {}
  
  virtual bool eval() const  = 0;

  virtual void PrintCut(std::stringstream *result) = 0;
};

// named variables, with plot ranges and binning

class ivar 
{
public:
  ivar()
  {}
  ivar(std::string tname, double tlower, double tupper,
      unsigned int tnbins=200) : name(tname), lower(tlower),upper(tupper),nbins(tnbins){}

  virtual ~ivar()
  {}

  std::string name;
  double lower;
  double upper;
  unsigned int nbins;
  std::vector<TH1D*> plots;
  
  TH1D* addPlot(std::string suffix)
  {
    std::string plotname=name;
    plotname.append(suffix);
    
    plots.push_back(new TH1D(plotname.c_str(),plotname.c_str(),nbins,lower,upper));
    return plots.back();
    
  }
  

  virtual double val()const {return 0;}
  
};

template<typename T>
class var : public ivar 
{
public:
  var(std::string tname, T* tvari, 
      double tlower, double tupper,
      unsigned int tnbins=200) : ivar(tname,tlower,tupper,tnbins), vari(tvari){}
  
  virtual ~var()
  {}
  

  T* vari;
  
  virtual double val() const {
    return (double)*vari;
  }
};

typedef var<double> dvar;
typedef var<float> fvar;


//////////////////////////////////////////////////////////////////////////////
/// cuts and ranges

template<typename op, typename T = double>
class cut : public IEval {
public:
  T* m_var;
  T m_val;
  
  cut(const std::string& the_name, const std::string& the_varname, T* var, T val) : m_var(var), m_val(val)
  {name=the_name; varname = the_varname;
    if(!std::is_same< T, typename std::decay<typename op::first_argument_type>::type >::value)
      printf("\033[0;36m%-20.20s\033[1;33m%-8.8s\033[0m template types of operator and var/val are not equal! Please have a look!\n","cutter.h:cut","WARNING:");
  }
  
  virtual ~cut()
  {}
  

  bool eval() const
  {
    return op()(*m_var,m_val);
  }

  void PrintCut(std::stringstream *result){
    *result << name << ":" << varname << ":<cut>:" << m_val << "\n";
  }
  
};


class range : public IEval{
public:
  double* m_var;
  double m_lower;
  double m_upper;

  range(const std::string& the_name, const std::string& the_varname, double* var,double lower, double upper) : 
    m_var(var), m_lower(lower), m_upper(upper)
  {name=the_name; varname = the_varname;}

  virtual ~range(){}
  

  bool eval() const
  {
    return (m_lower < *m_var) && (*m_var < m_upper);
  }
  
  void PrintCut(std::stringstream *result){
    *result << name << ":" << varname << ":<range>:" << m_lower << ":" << m_upper << "\n";
  }
  
  
};



// exclude range
class xrange : public IEval{
public:
  double* m_var;
  double m_lower;
  double m_upper;

  xrange(const std::string& the_name, const std::string& the_varname, double* var,double lower, double upper) : 
    m_var(var), m_lower(lower), m_upper(upper)
  {name=the_name; varname = the_varname;}

  virtual ~xrange(){}
  

  bool eval() const
  {
    return (*m_var < m_lower) || ( m_upper < *m_var);
  }
  
  void PrintCut(std::stringstream *result){
    *result << name << ":" << varname << ":<xrange>:" << m_lower << ":" << m_upper << "\n";
  }
  
};

// build an OR of two cuts
class orcut :  public IEval
{
public:
  IEval* cut1;
  IEval* cut2;
  orcut(std::string the_name, const std::string& the_varname, IEval* c1, IEval*c2) : cut1(c1),cut2(c2)
  {name=the_name; varname = the_varname;}
  
  bool eval() const 
  {
    return cut1->eval() || cut2->eval();
  }
}
;

  




//////////////////////////////////////////////////////////////////////////////////
//// CUTSET

class cutset {
public:

  cutset()
  {}

  ~cutset(){
    // clean up
    for (unsigned int i=0;i<m_variables.size();++i){
      delete m_variables[i];
      m_variables[i]=NULL;
    }
    for (unsigned int i=0;i<m_cuts.size();++i){
      delete m_cuts[i];
      m_cuts[i]=NULL;
    }

  }
  
  
  void addCut(IEval* cut){
    if(m_histos.size()>0){
      std::cerr << "Histos already initialized. Can't add another cuit" << std::endl;
      return;
    }
    m_cuts.push_back(cut);
  }

  
  void addVariable(ivar* variable){
    if(m_histos.size()>0){
      std::cerr << "Histos already initialized. Can't add another cuit" << std::endl;
      return;
    }
     m_variables.push_back(variable);
  }
  
    
  // call this after adding all cuts and variables!
  std::vector<TH1D*> initHistos(){
    // we need for each registered variable
    // a histo for fully cut
    // a histo for each allButOne cut
    // a histo for each allButOne anti-cut (later)
    for (unsigned int i=0; i<m_variables.size(); ++i) {
      // fully cut histo
      ivar* v=m_variables[i];
      m_histos.push_back(v->addPlot("AllCuts"));
      // loop over cuts
      for(unsigned int j=0;j<m_cuts.size();++j){
        IEval* c=m_cuts[j];
        // add histo name
        m_histos.push_back(v->addPlot(c->name));
        // add histo for anti cut
        std::string anti="_NOT_";anti.append(c->name);
        m_histos.push_back(v->addPlot(anti));
        
      } // end loop over cuts
      
    } // end loop over variables
    return m_histos;
    
  }
  
  // this is duplicating some code, unfortuntaly needed
  void histoNames(std::vector<std::string>& result){
     for (unsigned int i=0; i<m_variables.size(); ++i) {
       ivar* v=m_variables[i];
       std::string thename=v->name;thename.append("AllCuts");
       result.push_back(thename);
        for(unsigned int j=0;j<m_cuts.size();++j){
          IEval* c=m_cuts[j];
          thename=v->name;thename.append(c->name);
          result.push_back(thename);
          std::string anti=v->name;anti.append("_NOT_");anti.append(c->name);
          result.push_back(anti);
          
        }
     }
  }
  
  // check if cut is cutting on current variable (use name for this)
  bool checkSelfCut(IEval* cut, const std::string& name){
    return (cut->name.find(name) != std::string::npos);
  }
  
  unsigned int
  repairSelfCut(const std::vector<bool>& pass, std::vector<bool>& repaired, std::string& name)
  {
    repaired.clear();
    unsigned int passed=0;
    // check list of cuts
    for(unsigned int i=0;i<m_cuts.size();++i)
    {
      if(checkSelfCut(m_cuts[i],name))repaired.push_back(true);
      else(repaired.push_back(pass[i]));
      if(repaired.back())passed+=1;
    }
    return passed;
  }
  


  void fillHistos(){
    //std::cout<<m_variables[0].val()<<std::endl;
    std::vector<bool> passed, repaired;
    unsigned int npassed=eval(passed);
    unsigned int ncuts=m_cuts.size();
    if(npassed<ncuts-2)return;
    
    for(unsigned int i=0;i<m_variables.size();++i){
      ivar* v=m_variables[i];
      npassed=repairSelfCut(passed,repaired,v->name);
      if(npassed==ncuts){
        v->plots[0]->Fill(v->val());
        // loop over all but one plots // anit cuts don't get filled
        for(unsigned int j=0;j<ncuts;++j){
          if(v->plots[2*j+1]!=NULL)v->plots[2*j+1]->Fill(v->val());
        }
      }
      else { // fill the all-but-one plot for the cut that was the only one failing
             // exclude a cut on the variable itself
        unsigned int excemptcut=0;
        if(allButOne(repaired,npassed,excemptcut)){
          v->plots[2*excemptcut+1]->Fill(v->val());
          // also fill anticut
          v->plots[2*excemptcut+2]->Fill(v->val());
        }
        
      }
      
    } // end loopm over variables
  } // end fill histos
  
  
  
  // evaluate all cuts
  // return number of passed cuts
  unsigned int eval(std::vector<bool>& result){
    result.clear();
    unsigned int  pass=0;
    
    for(unsigned int i=0; i<m_cuts.size(); ++i){
      result.push_back(m_cuts[i]->eval());
      if(result.back())pass+=1;
      }
    return pass;
  }
 
  bool passall()
  {
    std::vector<bool> results;
    return eval(results)==m_cuts.size();
  }
  
  

  //  will return if allbutOne plot should be filled
  bool allButOne(const std::vector<bool>& cuts,   // cut[i] is the cut decision cut i
                 const unsigned int& npass,
                 unsigned int& excemptcut) // 
  {
    // note: all but one plots can only be filled if at most only one cut failed
   unsigned int ncuts=cuts.size();
    if(npass!=ncuts-1) return false;
    else
    {
      // check which one is it?
      for(unsigned int i=0; i<ncuts;++i){
        if(!cuts[i]){
          excemptcut=i;
          break;
        }
      } // end loop over cuts
      return true;
    }// endif 
  }
  
  
  //to save all cuts in an external file
  void SaveCuts(std::ofstream *out_stream){
    
    std::stringstream *cut_stream = new std::stringstream();
    
    //I would prefer iterators to loop over the cuts,
    //but there's a strange compiler error, mah...
    //for(std::vector<IEval*>::const_iterator cut = m_cuts.begin();
    //    cut != m_cuts.end(); ++cut){
    //  cut->PrintCut(cut_stream);
    //}  //loop over all cuts
    
    //loop over all cuts
    for(unsigned int i = 0; i < m_cuts.size(); ++i)
      m_cuts[i]->PrintCut(cut_stream);
    
    //print the cuts into the terminal
    std::cout << cut_stream->str() << std::endl;
    
    //save the cuts in the output stream
    *out_stream << cut_stream->str();
    
    return;
  }
  
  
  std::vector<IEval*> m_cuts;
  std::vector<ivar*> m_variables;
  std::vector<TH1D*> m_histos;
 
  
};

  
    



void testCut() 
{
  double variable = 1.0;
  
  cut<lss> mcut("cut1","var1",&variable,10);
  cut<grt> mgcut("cut2","var2",&variable,10);

  
  for(unsigned int i=0; i<10; i++)
  {
    variable+=2;
    std::cout << "Less: " << variable << " : " << mcut.eval() << std::endl;
    std::cout << "Greater: " << variable << " : " << mgcut.eval() << std::endl;
    


  }
  
  

}


void testAllButOne()
{
  cutset cs;
  
  std::vector<bool> cuts(5);
  cuts[0]=true;
  cuts[1]=true;
  cuts[2]=true;
  cuts[3]=false;
  cuts[4]=true;
  
  std::vector<bool> cuts2(5);
  cuts2[0]=true;
  cuts2[1]=true;
  cuts2[2]=true;
  cuts2[3]=true;
  cuts2[4]=true;

  std::vector<bool> cuts3(5);
  cuts3[0]=true;
  cuts3[1]=true;
  cuts3[2]=false;
  cuts3[3]=false;
  cuts3[4]=true;
  
  unsigned int ifail=0;
  if(cs.allButOne(cuts,4,ifail))std::cout<< "All but " << ifail << std::endl;
  
  


}

     

#endif 
