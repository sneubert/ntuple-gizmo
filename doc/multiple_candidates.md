# Documentation for multiple_candidates.cpp

For a given input tree or chain, the script writes an output tree of multiple candidates and a histogram of their multiplicities.<br>
The following (LHCb-specific) branches need to exist in the input tuple: `runNumber` and `eventNumber` as unsigned int and unsigned long long respectively.
As event numbers are not unique in LHCb, both information is needed to make a unique event number (which is `10e+9*runNumber+eventNumber`).
Multiple candidates are thus defined as entries in the input tree/chain which have the same unique event number<br>
It's runtime options are parsed entirely from the command line. I.e. no config file needed

## Command line options
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with @<filename> (similar to hadd functionality))
- `-o` : output file
- `-t` : input `TTree` name
- `-w` : output `TTree` name (optional, default: input tree name + `Mult`)
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions are either branchnames which should be written to the output file (separated by `;`) or `friendfile(s):friendtree` combinations. More than 1 file:tree combination can be used (see syntax from `-i` option).
If no branchnames are specified, all are persisted.

## Note on performance
The script is single-threaded. Most of the time is spent loading the input chain into memory in order of unique event numbers rather than it's usual tree-index.
Although it is only loaded when a multiple candidate is found, a large speedup can be achieved when only the branches which are really needed later are specified. An example:

```
ntuple-gizmo/build/bin/multiple_candidates -d $DUMPDIR -i forWs/hists_secondaries.root -o MultipleCandidates.root -t TupleD0ToKpipipi_RS/DecayTree 'Pislow_PT;Pislow_ETA;Pislow_PHI;K_PT;K_ETA;K_PHI;Pi_SS_PT;Pi_SS_ETA;Pi_SS_PHI;Pi_OS1_PT;Pi_OS1_ETA;Pi_OS1_PHI;Pi_OS2_PT;Pi_OS2_ETA;Pi_OS2_PHI' -w t
```
takes `Real time 0:00:02, CP time 1.680`
while
```
ntuple-gizmo/build/bin/multiple_candidates -d $DUMPDIR -i forWs/hists_secondaries.root -o MultipleCandidates.root -t TupleD0ToKpipipi_RS/DecayTree -w t
```
with 772 branches takes
`Real time 0:01:08, CP time 67.970`
