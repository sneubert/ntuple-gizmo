# Documentation for elwms, a versatile tool for creating trimmed samples, workspaces or histograms and applying TMVA weights

Script for using `RDataFrame` with property trees.

***Requirements*** A C++17 compiler, ROOT > 6.17/01. Make sure to `export OPENBLAS_MAIN_FREE=1` to use implicit multithreading

## Command line options
The options which can be given to the executable are
- `-c` : location of the config file (automatic replacing and appending enabled)
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with `@<filename>` (similar to hadd functionality))
- `-m` : combination(s) of `<weightfile (xml format):BDT response name>`. More than one of these combinations can be given, separated by a semicolon.
- `-o` : output file (since there can be more than 2 output files, the trimmed tree is put into `<working directory>/forWs/<outputfilename>`, a file containing a RooWorkspace is put in `<working directory>/forWs/<outputfilename>` and histograms are saved in `<working directory>/hists/<outputfilename>`)
- `-r` : name of output `RooDataSet` (only if ws_vars are given in config file)
- `-t` : input `TTree` name
- `-v` : verbosity (1-3, optional)
- `-w` : name of output `RooWorkspace` (only if ws_vars are given in config file)
- `-h` : help
- nonoptions are optional `friendfile(s):friendtree` combinations and/or `<variable->transformation>` combinations. Note that you can also add friend-chains, where the syntax for `friendfile(s)` is the same as for the `-i` option

### Hints for running with multiple input files
- Don't forget the ticks (`''`) when using the semicolon separated list.
- Don't mix input files that should be prepended with `workdir` with files at absolute paths. Give all of them with absolute paths instead.

### Hints for the -m option
Input variables to the TMVA::Reader are parsed from the xml file. This means that all variables have to be known internally to RDataFrame.
This means that they don't have to be part of the ouput variables (variables node), but present in the input tree, or be among the (internal) transformation variables.
For example, the following code would define `myMVAInputVar` but not add it to the output root file
```
variables {
  dummy_i {
    nn myMVAInputVar
    tr "log10(myTreeVar)"
  }
}
```

## The config file
Check out the [template](config/template_tree_trimmerRDF.info)!
The following can be given in a config file to elwms:

- threads : ( `unsigned int`, default: all available cores ) Number of threads used by ROOT's implicit multithreading. If this is 0, multithreading is disabled

- variables: Variables which are read in by `RDataFrame` (all by default) <br>
  The variable names are key or node themselves. By default, the variable is saved as it is. If the name contains `[0]`, it is automatically renamed like  `<name>[0]` -> `<name>Z`. The name `dummy` is reserved for creating a new variable with a transformation. The name `dummy_i` is reserved for "internal" variables that are needed for further define actions, but should not be saved to the output.<br>
  Optional fields of the nodes are:
      - `nn` to rename the variable. This calls [`Define`](https://root.cern.ch/doc/master/classROOT_1_1RDF_1_1RInterface.html#aefd0d480704c0c39c599c28666c713d2) with `new_name` as name and the node-name as expression
      - `tf` is a single line C++ statement as string to transform the variable. This needs `new_name`.
      - `to_output 0` to address this variable, but not write it to the output. This is useful if the variable is needed for calculations like the beta variables or invariant masses, but not needed in the following.

- misIDMbeta: Triggers calculation of subcombinations of invariant masses and single particle momentum asymmetries. It is a node containing the names of final state particles. It will append their names by `_PX,_PY,_PZ,_PE` to build the LorentzVectors, so make sure they are on the input tuple.

- Filter : ( `string`, optional ) Filter action called after all transormations (so that cuts on transformed variables can be used)

- outtreename: Name of the tree in the output file (it's the inputname by default)

- LumiTuple: takes an integer bool value dictating whether to save another root file (in `<workdir>/forWs/Lumi_<outfilename>`) containing a tree with the lumi information

- LumiTupleTree: (`string`, optional) Location of Lumi tree in input file. Default is `GetIntegratedLuminosity/LumiTuple`. Only active if LumiTuple is True

- AddBranchMode: (`bool`,default false) Adds variables defined in `variables` to the input tree, rather than persisting only those in given in `variables`

- remove_nan_inf: (`bool`,default false) rejects events where at least one value of the variables defined in the `variables` node (which is going to be saved in the output tree) is NaN or inf.

- create_indexlist: (`bool`,default false) creates a branch with ascending integers. It starts incrementing after all Filter/Define actions

- add_lvs_to_output: (`bool`,default false) persists TLorentzVectors that were used to calculate beta and M (only works if misIDMbeta is given)

- ws_vars:  (`property_tree node`) Similar to `variables` node described above. This node triggers creation of a `RooDataSet` in a `RooWorkspace`. <br>
                                   Their name is given as object identifier, optional parameters are `low`, `high` to specify the range. The default is `-inf` to `inf` <br>
                                   No transformations are possible here, but previously internal variables can still be added. <br>
                                   If this node is added, more options become available:
    - kdepdfs  : Pdfs added to workspace that use the `RooKeysPdf` class. <br>
      The name of the iterable object (lets call it _itername_) will be the name of the `RooKeysPdf`. <br>
      If the resolution node is given, the name will be appended with an additional `xg`. <br>
      When setting up the model with factory code use `RooKeysPdf::itername` or `RooFFTConvPdf::iternamexg`

        - filename   : ( `string`            , no default                ) Location of filename with shape for `RooKeysPdf` (absolute or relative to path given with `-d`)
        - treepath   : ( `string`            , no default                ) Location of tree within given file
        - obsinfile  : ( `string`            , default `observable.name` ) Name of branch/leaf where the shape will be extracted from. Defaults to the name of the observable specified above
        - observable : ( `string`            , optional                  ) Name of the observable that the `RooKeysPdf` will be created in (By default this is the `RooRealVar` created by `observable.name`) <br>
          **ATTENTION** It is highly recommended to set a range for the observable in the `variables` or `variables_override` node
        - mirror     : ( `RooKeysPdf::Mirror`, default `3 (MirrorBoth)`  ) Mirror-option in `RooKeysPdf` ctor. Chose an integer number between 0 and 8 for the following options <br>
          `enum Mirror { NoMirror = 0, MirrorLeft, MirrorRight, MirrorBoth, MirrorAsymLeft, MirrorAsymLeftRight, MirrorAsymRight, MirrorLeftAsymRight, MirrorAsymBoth };`
        - rho        : ( `double`            , default `1.0`             ) Should only be adjusted in extreme situations (very strong local peaks, see [arXiv:hep-ex/0011057](https://arxiv.org/abs/hep-ex/0011057 "arXiv:hep-ex/0011057"))
        - fold       : ( `bool`              , default `1 (true)`        ) Set folding for every pdf individually

                 kdepdfs {
                   LcDstgK { ;1st item in kdepdfs-list
                     filename   "LcD0gK_mc.root"
                     treepath   "smalltree"
                     obsinfile  "Lb_Cons_M"
                     fold       0
                   }
                   LcDstpi0K { ;2nd item in kdepdfs-list
                     filename   "root://eoslhcb.cern.ch//eos/lhcb/user/m/mstahl/Lb2LcD0K/XFeedMC/15196400.root"
                     treepath   "MCDecayTreeTuple/MCDecayTree"
                     obsinfile  "Lb_LcD0K_M"
                   }
                   Lc_from_Lb2LcDstgK { ;KDE in different observable (KDE defined in `mLb` and read from `Lc_M`)
                     filename   "LcD0gK_mc.root"
                     treepath   "smalltree"
                     obsinfile  "Lc_M"
                     observable "mLc"
                     fold       0
                   }
                 }


The following options work, but might change in the future:
- outfopt: pass option string to ctor of TFile for the output-file (https://root.cern.ch/doc/master/classTFile.html#aadd8e58e4d010c80b728bc909ac86760). default is "recreate"
- outcompalg: [compression algorithm](https://root.cern.ch/doc/master/namespaceROOT.html#a60945f6fe634f9f3be9872e57bf87f2e) for the output file (default is inherited)
- outcomplvl: [compression level](https://root.cern.ch/doc/master/namespaceROOT.html#a0b09a8c4888c9bb4388950ce7d5041b6) for the output file (default is inherited)

### Saving histograms
The elwms script includes the functionality of chain2histRDF. For writing config files, please have a look at the [chain2histRDF doc](doc/chain2histRDF.md), the [stuff2hist doc](doc/stuff2hist.md) and the [template](config/template_chain2histRDF.info) for chain2hist.
To maintain some compatibility with `stuff2hist`, this script also allows for 2 types of draw commands:
- Draw, cut and weight to mimic the `Draw()` function (only Draw is mandatory)
- or var, nbins, min, max, yvar, nbinsy, ymin, ymax, zvar, nbinsz, zmin, zmax, histname and weight to have a easyly understandable configuration. Here only var is mandatory

The `Draw` and `var` commands are placed in a node that has to be called `hists`. <br>
Using weights: note that there is a difference between this script and `stuff2hist`, which only allowed to define a single global weight for all histograms in the config file.
