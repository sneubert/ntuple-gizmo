# Documentation for tree_trimmerRDF, a versatile tool for trimming trees or chains

Script for using `RDataFrame` with property trees.

## Command line options
The options which can be given to the executable are
- `-c` : location of the config file (automatic replacing and appending enabled)
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with @<filename> (similar to hadd functionality))
- `-o` : output file
- `-t` : input `TTree` name
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions are optional `friendfile:friendtree` combinations (currently not working -- ROOT 6.17) and/or <variable@transformation> combinations

### Hints for running with multiple input files
- Don't forget the ticks (`''`) when using the semicolon separated list.
- Don't mix input files that should be prepended with `workdir` with files at absolute paths. Give all of them with absolute paths instead.

## The config file
Check out the [template](config/template_tree_trimmerRDF.info)!
The following can be given in a config file to tree_trimmer:

- threads : ( `unsigned int`, default: all available cores ) Number of threads used by ROOT's implicit multithreading

- variables: Variables which are read in by `RDataFrame` (all by default) <br>
  The variable names are key or node themselves. By default, the variable is saved as it is. If the name contains `[0]`, it is automatically renamed like  `<name>[0]` -> `<name>Z`. The name `dummy` is reserved for creating a new variable with a transformation. The name `dummy_i` is reserved for "internal" variables that are needed for further define actions, but should not be saved to the output.<br>
  Optional fields of the nodes are:
      - `nn` to rename the variable. This calls [`Define`](https://root.cern.ch/doc/master/classROOT_1_1RDF_1_1RInterface.html#aefd0d480704c0c39c599c28666c713d2) with `new_name` as name and the node-name as expression
      - `tf` is a single line C++ statement as string to transform the variable. This needs `new_name`.
      - `to_output 0` to address this variable, but not write it to the output. This is useful if the variable is needed for calculations like the beta variables or invariant masses, but not needed in the following.

- misIDMbeta: Triggers calculation of subcombinations of invariant masses and single particle momentum asymmetries. It is a node containing the names of final state particles. It will append their names by `_PX,_PY,_PZ,_PE` to build the LorentzVectors, so make sure they are on the input tuple.

- Filter : ( `string`, optional ) Filter action called after all transormations (so that cuts on transformed variables can be used)

- outtreename: Name of the tree in the output file (it's the inputname by default)

- AddBranchMode: (`bool`,default false) Adds variables defined in `variables` to the input tree, rather than persisting only those in given in `variables`

- remove_nan_inf: (`bool`,default false) rejects events where at least one value of the variables defined in the `variables` node (which is going to be saved in the output tree) is NaN or inf.

- create_indexlist: (`bool`,default false) creates a branch with ascending integers. It starts incrementing after all Filter/Define actions

- add_lvs_to_output: (`bool`,default false) persists TLorentzVectors that were used to calculate beta and M (only works if misIDMbeta is given)

The following options work, but might change in the future:
- outfopt: pass option string to ctor of TFile for the output-file (https://root.cern.ch/doc/master/classTFile.html#aadd8e58e4d010c80b728bc909ac86760). default is "recreate"
- outcompalg: [compression algorithm](https://root.cern.ch/doc/master/namespaceROOT.html#a60945f6fe634f9f3be9872e57bf87f2e) for the output file (default is inherited)
- outcomplvl: [compression level](https://root.cern.ch/doc/master/namespaceROOT.html#a0b09a8c4888c9bb4388950ce7d5041b6) for the output file (default is inherited)
