# Documentation for tree_trimmer.cpp

The script is basically a implementation of `TTree::CopyTree()` on a `TEntryList` that can handle friend-trees.

## Command line options
The options which can be given to the executable are
- `-c` : location of the config file
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with @<filename> (similar to hadd functionality))
- `-o` : output file
- `-t` : input `TTree` name
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions are `friendfile(s):friendtree` combinations. Can take more than 1 file (see syntax from `-i` option)

## The config file
The following can be given in a config file to tree_trimmer:

- basiccuts : ( `string`, default `""`   )  Cut with wich the dataset will be read in (`TCut` format)

- noImplicitCuts : optional. If given the ranges (if given) will be ignored

- subsample : optional. Nodes:
      - `fraction` of events to be kept (mandatory)
      - `seed` for random choice of events, default:6666 (optional)

- variables: Variables which are saved in the ouput tree. <br>  
  The node names will be used to set the branch address (i.e. the node names are the variable names as they are given in the input tree). <br>
  Optional fields of the nodes are:
      - `low` and `high` to specify the range (*mandatory if noImplicitCuts is not switched on*). **ATTENTION** be aware that the given range is an implicit cut on the data
      - `new_name` to rename (and possibly transform - see below) the variable

- outtreename: Name of the tree in the output file (it's the inputname by default)
- outfopt: pass option string to ctor of TFile for the output-file (https://root.cern.ch/doc/master/classTFile.html#aadd8e58e4d010c80b728bc909ac86760). default is "recreate"



### Special commands to manipulate variables
Variables can be renamed and/or transformed easily by the `new_name` key in the `variables`-list.
Currently, the following transformations are parsed: `log_` (log10(x)), `sqrt_`, `atan_` and `NNtfd_` (log10(x/(1-x))).

### A full example:

        basiccuts "K_ProbNNk > 0.01 && D0BDT > -0.03 && LcBDT > -0.03"

        variables {
            Lb_PT {  ;1st item in variables-list
              low  2
              high 5 ;the implicit cuts apply to the tansformed variables
              new_name "log_Lb_pt"
            }
            Lb_ETA { ;2nd item in variables-list
              low 1.9
              high 5.5
            }
        }
