# Documentation for applyMVA
This script allows for applying a TMVA trained MVA to a ntuple using xml weight-files. It makes a new root file with the MVA response as leaf.
This file can be used as friend-tree of the input tuple or files can be combined, e.g. with the tree_trimmer script.

## Arguments for the executable
- `-c` : config file
- `-d` : work directory
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with @<filename> (similar to hadd functionality))
- `-m` : TMVA weight-file name
- `-f` : factory name
- `-o` : ouput file name
- `-t` : input tree name
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions are `friendfile(s):friendtree` combinations. Can take more than 1 file (see syntax from `-i` option)

## Example config file
[apply generic D0 BDT](https://gitlab.cern.ch/lhcb-bandq-exotics/Lb2LcD0K/blob/master/config/MVA/D0BDTconfig.info)

# Writing a config file
Mandatory:
- `tasks.<factory_name>.variables` child, as in the training script (it's actually easiest to use the same config file for training and application).
- `response_name` name of the MVA response that will be written to the output file

Optional:
 - `tasks.<factory_name>.spectators` child, to correctly address spectator variables in the xml file.
 - `rarity` uses TMVAs rarity transform (needs CreateMVAPdfs in TMVA training options). This means that the background response is made uniform on the TMVA test sample, so that the cut can be interpreted as probability
 - `tasks.<factory_name>.ignore_missing_variables` bool, ignores variables which are given to `variables` or `spectator` node, but are not in the xml file
